# frozen_string_literal: true

# Set environment variable to set a security token that will be sent for a user
# ENV['admin_token'] = 'test'

# Add users to LeapSalesforce context. First user is the default
module LeapSalesforce
  Users.add User.new :admin, 'samuel.garratt@brave-otter-ttxype.com'
  Users.add User.new :not_there, 'not.there@brave-otter-ttxype.com'
  Users.add User.new :cet, 'test.cet@brave-otter-ttxype.com'
  Users.add User.new :extra, 'test.extra@brave-otter-ttxype.com'
end

ENV['SF_USERNAME'] = 'samuel.garratt@brave-otter-ttxype.com'
