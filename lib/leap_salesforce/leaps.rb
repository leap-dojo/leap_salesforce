# frozen_string_literal: true

# Create a Leaps alias to LeapSalesforce as it's faster to type.
# This will not be automatically required in case as 'Leaps' may be defined elsewhere
require 'leap_salesforce'
Leaps = LeapSalesforce
