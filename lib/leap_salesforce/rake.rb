# frozen_string_literal: true

require 'leap_salesforce'
Dir.glob(File.join(__dir__, 'rake', '*.rake')).each(&method(:import))
