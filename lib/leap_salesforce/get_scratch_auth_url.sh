# Get scratch ORG logging in via Auth url
#echo $org_auth_url > $file
sfdx force:auth:sfdxurl:store --sfdxurlfile "$file" --alias HubOrg --json
sfdx force:org:display --target-org "$SCRATCH_ORG_ALIAS"