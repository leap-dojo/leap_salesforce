# frozen_string_literal: true

# adding additional methods to inbuit time object
class Time
  # @return [String] String represents time in salesforce format
  def salesforce_format
    strftime('%Y-%m-%d')
  end

  # @param [String] date_string Date returned from salesforce
  # @return [Time] Time object created from salesforce date String
  def self.parse_salesforce(date_string)
    mktime(*date_string.split('-'))
  end
end
