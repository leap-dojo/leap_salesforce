# frozen_string_literal: true

require 'nori/core_ext'
require 'humanize'
require 'active_support/isolated_execution_state'
require 'active_support/core_ext/string'
require 'date'
require 'json'

class MockHandler < Soaspec::RestHandler
  base_url 'mock_url'
end

# Override string object to provide convenience methods for Strings
class String
  # @return [Array] List of keywords reserved for FactoryBot
  FACTORY_WORDS = ['sequence'].freeze
  # @return [Array] List of ruby keywords. Some words are only key to FactoryBot like 'sequence'
  KEYWORDS = %w[BEGIN END __ENCODING__ __END__ __FILE__ __LINE__ alias and begin break case class def defined?
                do else elsif end ensure false for if in module next nil not or redo rescue retry return
                self super then true undef unless until when while yield private].freeze
  Soaspec.api_handler = MockHandler.new
  SOQLDATA_METHODS = Exchange.new.public_methods.collect(&:to_s)

  # @note This removes '?' which is allowed in a method but is not desired for automatic creation of their names
  #   in the way that is being used here
  # @return [String] Convert string to something that would be friendly to use as in Ruby
  def to_ruby_friendly
    tr('&|=', '_').gsub('<', '_lt_').gsub('>', '_gt_')
                  .remove_macrons
                  .gsub(/\s+/, '_')
                  .gsub(/\W/, '') # Remove any other special characters
                  .handle_initial_characters # Previous step could have removed all characters
                  .humanize_numbered_string
  end

  # @example Remove stripped characters
  #   ''.handle_initial_characters # => 'X'
  # @example Remove leading underscores
  #   '_leading'.handle_initial_characters # => 'leading'
  # @return [String] If all characters were stripped out, put a placeholder 'X'.
  #   If starts with underscore remove it
  def handle_initial_characters
    if size.zero?
      +'X'
    else
      self[0] = '' if self[0] == '_'
      self
    end
  end

  # @return [String] Convert String to form a class could use
  def to_class_name
    camelize.to_ruby_friendly.tr('_', '').camelize
  end

  # @return [String] Remove many complex characters to make a key for a Ruby Hash that is usable in a method/key
  def to_key_name
    to_ruby_friendly.snakecase.gsub(/__+/, '_')
  end

  # @return [String] Return a key name that is unique to the hash
  def to_key_name_for(hash)
    key_name = to_key_name
    index = 0
    while hash[key_name]
      if ('1'..'9').cover? key_name[-1]
        key_name[-1] = (key_name[-1].to_i + 1).to_s
      else
        key_name += '_1'
      end
      break if (index += 1) >= 9
    end
    key_name
  end

  # Convert string starting with number to a human string
  def humanize_numbered_string
    self[0] = ('0'..'9').cover?(self[0]) ? self[0].to_i.humanize : self[0]
    self
  end

  # @return [String] Return string without macrons, used for filenames and ruby methods
  def remove_macrons
    tr('ā', 'a').tr('ē', 'e').tr('ō', 'o').tr('ī', 'i').tr('ū', 'u')
  end

  # A ruby friendly method name that is not taken by keywords like 'class', 'alias'
  # @return [String] Ruby friendly name that can be used as method name
  def unused_ruby_name
    name = to_key_name
    name.keyword? ? "my_#{name}" : name
  end

  # @return [String] Camel case string that is not taken by keywords like 'class', 'alias'
  # @param [Symbol] type :upper or :lower, default :upper
  def unused_camel_name(type = :upper)
    unused_ruby_name.camelize(type)
  end

  # @return [String] Zulu time stamp for string
  def to_zulu_date_string
    Time.parse(self).strftime('%Y-%m-%dT%H:%M:%S.000Z')
  end

  # @return [Boolean] Whether string can be parsed into a time
  def type_of_time?
    Date.strptime(self, '%Y-%m-%d')
  rescue ArgumentError
    false
  else
    true
  end

  # Convert Ruby String representation of Array to SoqlArray
  # @return [String] String representation for use in Soql IN query
  def to_soql_array
    arr = JSON.parse(self)
    unless arr.is_a? Array
      raise ArgumentError, "Cannot convert #{self} into Array" \
      "Called at #{caller_locations[0]}"
    end
    joined = arr.join("', '")
    "('#{joined}')"
  end

  # @return [Boolean] Whether string is a Ruby keyword
  def keyword?
    KEYWORDS.include?(self) || FACTORY_WORDS.include?(self) || SOQLDATA_METHODS.include?(self)
  end
end
