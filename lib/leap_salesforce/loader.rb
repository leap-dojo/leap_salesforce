# frozen_string_literal: true

module LeapSalesforce
  # For loading dependent code based on configuration
  module Loader
    # @return [String] Location of leap_salesforce YAML file
    LEAP_CONFIG_FILE = '.leap_salesforce.yml'
    class << self
      # Set LeapSalesforce property if it exists
      def set_leaps_property(key, value)
        if LeapSalesforce.respond_to? "#{key}="
          if %w[SF_CONSUMER_KEY client_id client_secret password].include? key
            LeapSalesforce.logger.warn "Secret key '#{key}' should be in non version" \
            " controlled #{LeapSalesforce::CREDENTIAL_FILE} not in #{LEAP_CONFIG_FILE}"
          end
          LeapSalesforce.send("#{key}=", value)
        else
          LeapSalesforce.logger.warn "No property for '#{key}' from '#{LEAP_CONFIG_FILE}'"
        end
      end

      # Load configuration file and set properties based on it
      def load_config_file
        if File.exist? LEAP_CONFIG_FILE
          leap_config = YAML.load_file LEAP_CONFIG_FILE
          LeapSalesforce.soql_objects = leap_config.delete('soql_objects')
          leap_config.each { |key, value| set_leaps_property(key, value) }
        else
          LeapSalesforce.logger.warn "No config file found at '#{LEAP_CONFIG_FILE}' for Leap Salesforce"
        end
      end
    end
  end
end
