# frozen_string_literal: true

module LeapSalesforce
  # Any error specific to LeapSalesforce
  class Error < StandardError; end

  # Error related to handling a request
  class RequestError < StandardError; end

  # Error related to handling a response
  class ResponseError < StandardError; end

  # Error related to setup of test automation suite
  class SetupError < Error; end

  # Error related to Test Users
  class UserError < Error; end
end
