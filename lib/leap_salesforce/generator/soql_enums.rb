# frozen_string_literal: true

require_relative 'soql_objects'
require_relative '../soql_data/soql_data'

module LeapSalesforce
  # Generators for creating code
  module Generator
    # Creates Soql Enumerations / Picklists from objects in LeapSalesforce.soql_objects
    class SoqlEnums
      include Generator

      # @return [String] Folder where enumeration objects are stored
      if LeapSalesforce.enum_folder
        ENUM_FOLDER = LeapSalesforce.enum_folder
      else
        ENUM_FOLDER = File.join(LeapSalesforce.lib_folder, 'metadata', 'enum').freeze
      end

      # Create Soql enumerations for all objects specified in .leap_salesforce.yml
      def create
        if LeapSalesforce.soql_objects.empty?
          raise LeapSalesforce::SetupError, 'LeapSalesforce.soql_objects is empty. ' \
        'Please set the list of objects you want to create enums for in .leap_salesforce:soql_objects ' \
        'and then run task "leaps:create_soql_objects" to create objects'
        end
        reset_enum_folder
        LeapSalesforce.soql_objects.each { |entity| create_picklists_for entity }
        cleanup_files_created
      end

      # Create files for each picklist found for that entity
      # @param [LeapSalesforce::SoqlData] entity An object representing an object in Salesforce
      #   Object inheriting from SoqlData that has picklists underneath it
      def create_picklists_for(entity)
        soql_object = entity # LeapSalesforce.soql_objects.find { |so| so.class_name == entity.to_s }
        @soql_data_object = Class.new(SoqlData) do
          soql_object soql_object.backend_name
        end
        @entity_name = entity.class_name
        unless soql_object
          LeapSalesforce.logger.warn "Could not find soql object for '#{@entity_name}', ignoring" 
          return
        end
      
        unless soql_object.create_enum != false
          puts "Skipping picklists for #{@entity_name}"
          return
        end
        puts "Creating picklists for #{@entity_name}"
        
        picklists = @soql_data_object.picklists
        puts "#{picklists.count} picklists found"
        picklists.each do |picklist|
          if soql_object.excludes?(picklist)
            puts "Excluding picklist '#{picklist}'"
          else
            generate_picklist_file picklist
          end
        end
      end

      # Generate file for a picklist from it's metadata
      def generate_picklist_file(picklist)
        @picklist = picklist
        @enum_name = picklist.to_class_name
        picklist_name = picklist.to_key_name
        values = @soql_data_object.picklist_for(picklist)
        @enum_values = {}
        values.each do |value|
          @enum_values[value.to_key_name_for(@enum_values)] = value
        end
        if LeapSalesforce.language == "ruby"
          content = read_template 'picklist.rb.erb', binding
          file = File.join(ENUM_FOLDER, @entity_name.to_s.snakecase, "#{picklist_name}.rb")
        elsif LeapSalesforce.language == 'java'
          content = read_template 'picklist.java.erb', binding
          file = File.join(ENUM_FOLDER, @entity_name.to_s, "#{@enum_name}.java")
        else
          raise LeapSalesforce::SetupError, "Unsupported language '#{LeapSalesforce.language}'"
        end
        generate_file file, content
      end

      # Reset state of Enum folder so that enums that no longer exist do not persist
      def reset_enum_folder
        FileUtils.rm_rf ENUM_FOLDER
      end

      # Clean up files generated for all picklists
      def cleanup_files_created
        return unless LeapSalesforce.language == "ruby" 

        `rubocop -A #{ENUM_FOLDER} --display-only-fail-level-offenses --enable-pending-cops`
      end
    end
  end
end
