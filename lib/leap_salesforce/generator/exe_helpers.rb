# frozen_string_literal: true

require 'io/console'
require 'rest-client'
require 'soaspec'

module LeapSalesforce
  # Helpers for LeapSalesforce executable
  module ExeHelpers
    # Return input for query
    # @param [String] query Query to ask on terminal
    # @return [String] User input to store
    def input_for(query, color = nil)
      if color
        puts query.colorize(color)
      else
        puts query
      end
      $stdin.gets.strip
    end

    # Retrieve environment and ensure that environment can be connected to
    def verify_environment
      unless LeapSalesforce.sfdx
        LeapSalesforce.environment = options[:environment] || input_for('Enter the environment you want to set things up for ' \
  '(This will be parameterised so can be changed later). For production used "prod". Otherwise preference is to use the' \
  ' part of the URL that is unique for the environment')
      end
      LeapSalesforce.salesforce_reachable?
      puts "\u2713 Connection to #{LeapSalesforce.general_url} successful".colorize :green
    end

    # Retrieve OAuth credentials and verify that they're correct
    def verify_oauth
      unless LeapSalesforce.sfdx
        LeapSalesforce.client_id = options[:client_id] || input_for('Client id (Customer Id)')
        LeapSalesforce.client_secret = options[:client_secret] || $stdin.getpass('Client secret (Consumer Secret)')
        LeapSalesforce.password = options[:password] || $stdin.getpass('Password (Recommendation is that 1 password' \
  ' be shared across all test users to be easier to manage):')
      end
      LeapSalesforce.api_user = ERB.new(options[:username] || input_for('Salesforce username. It is ideal to start with a System admin' \
  ' so that any necessary metadata can be read. More users can be added later. You can use ERB to make name' \
  ' vary according to environment (e.g., test.user@<%= LeapSalesforce.environment %>.my.company.com)')).result(binding)
      LeapSalesforce.oauth_working?
    end

    # Ask user to enter parameters specific to their Salesforce environment
    def query_for_parameters
      puts 'Please enter the following information to generate files for a new' \
      ' leap_salesforce testing repo'.colorize(:green)
      verify_environment
      verify_oauth
      @user_key = options[:user_key] || input_for('Enter a key to refer to this user (This will be stored as a Symbol)').delete(':')
    end
  end
end
