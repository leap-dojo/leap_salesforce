# frozen_string_literal: true

require 'fileutils'
require 'leap_salesforce'
require_relative 'default'
require_relative 'generator'

module LeapSalesforce
  # Generators for creating code
  module Generator
    # Creates SoqlObjects and related modules
    class SoqlObjects
      include Generator

      SOQL_DATA_FOLDER = LeapSalesforce.language == 'java' ? 'SoqlObjects' : 'soql_data'
      SOQL_OBJECT_FOLDER = File.join(LeapSalesforce.lib_folder, SOQL_DATA_FOLDER).freeze
      FACTORY_FOLDER = File.join(LeapSalesforce.lib_folder, 'factories').freeze

      # Create temporary class for building API requests on
      def ensure_class_defined
        @soql_class = Object.const_get @soql_object.class_name
      rescue NameError
        @soql_class = Object.const_set @soql_object.class_name, Class.new(SoqlData)
        @soql_class.instance_variable_set(:@soql_object_name, @soql_object.backend_name)
      end

      # Generate Soql object file
      def generate_soql_object
        ensure_class_defined
        desc = @soql_class.description
        unless desc.soql_fields?
          raise LeapSalesforce::SetupError, "Error producing Soql data for '#{@soql_class}', soql table " \
          " '#{@soql_object.backend_name}'. Error message: '#{desc.error_message}'"
        end
        return unless LeapSalesforce.language == 'ruby'

        content = read_template 'soql_object.rb.erb', binding
        file = File.join(SOQL_OBJECT_FOLDER, "#{@soql_object.reference}.rb")
        generate_file file, content, overwrite: false
      end

      # Generate module with fields for Soql Object
      def generate_field_module
        if LeapSalesforce.language == 'ruby'
          field_content = read_template 'soql_object_field_names.rb.erb', binding
          field_file = File.join(SOQL_OBJECT_FOLDER, "#{@field_name_file}.rb")
        elsif LeapSalesforce.language == 'java'
          field_content = read_template 'soql_object_field_names.java.erb', binding
          field_file = File.join(SOQL_OBJECT_FOLDER, "#{@soql_object.class_name}.java")
        else
          raise LeapSalesforce::SetupError, "Unsupported language '#{LeapSalesforce.language}'"
        end
        generate_file field_file, field_content
      end

      # Generate factory for mass generating objects using FactoryBot
      def generate_factory
        return unless LeapSalesforce.language == 'ruby'

        @default_fields, @other_fields = filter_fields
        content = read_template 'factory.rb.erb', binding
        file = File.join(FACTORY_FOLDER, "#{@soql_object.reference}.rb")
        generate_file file, content, overwrite: false
      end

      # Generate soql_object and it's related modules, factories
      def create_all
        puts "Generating Soql objects for #{LeapSalesforce.soql_objects.count} objects"
        LeapSalesforce.soql_objects.each do |soql_object| # Soql object is now special class
          # extract_info_from_object soql_object
          @soql_object = soql_object
          @field_name_file = "#{soql_object.reference}_field_names"
          generate_soql_object
          generate_field_module
          generate_factory
        end
        return unless LeapSalesforce.language == 'ruby'

        `rubocop -A #{SOQL_OBJECT_FOLDER} --enable-pending-cops`
        `rubocop -A #{FACTORY_FOLDER} --enable-pending-cops`
      end

      private

      # @return [Array] Returns Array with first list being default fields for Factory and the 2nd being
      #   all other fields
      def filter_fields
        all_fields = @soql_class.fields
        if Default::Fields.respond_to? @soql_object.reference
          all_fields.partition { |field| Default::Fields.send(@soql_object.reference).include? field['name'] }
        else
          [[], all_fields]
        end
      end
    end
  end
end
