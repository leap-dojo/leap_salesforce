# frozen_string_literal: true

module LeapSalesforce
  module Default
    # Defaults for various objects within a Factory
    module Factory
      class << self
        # @return [String] String to set Factory field to value specified
        def set(value, use_quotes: nil)
          if use_quotes
            "#{@field['label'].unused_ruby_name} { '#{value}' }"
          else
            "#{@field['label'].unused_ruby_name} { #{value} }"
          end
        end

        # @return [String] Default value for Factory
        def value_for(field, class_name)
          @field = field
          case field['type']
          when 'string' then set("Faker::Lorem.paragraph_by_chars(number: #{field['length']})")
          when 'id' then set('Best to not hard code this', use_quotes: true)
          when 'boolean' then set('true')
          when 'picklist' then set("#{class_name}::#{field['label'].to_class_name}.sample")
          when 'reference'
            return set(%{User.find(CreatedDate: "<#{0.days.ago}").id}) if field['name'] == 'OwnerId'

            soql_obj = LeapSalesforce.soql_objects.find { |so| so.backend_name == field['relationshipName'] }&.reference
            if soql_obj
              "association :#{@field['label'].unused_ruby_name}, factory: :#{soql_obj}"
            else
              "# Please add #{field['relationshipName']} to .leap_salesforce.yml (if it's a table) to create association for #{field['relationshipName']}"
            end
          else
            set("Content depending on #{field['type']}", use_quotes: true)
          end
        end
      end
    end

    # Contains default fields for various out of the box entities to be included in Factory
    module Fields
      class << self
        def contact
          %w[FirstName LastName]
        end

        # @return [Array] List of backend field names to be populated by default for account
        def account
          ['Name']
        end
      end
    end
  end
end
