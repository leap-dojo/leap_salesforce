# frozen_string_literal: true

namespace :leaps do
  desc 'Create Enumeration objects for picklists from Salesforce Metadata'
  task :create_enums do
    LeapSalesforce::Users.execute_as_if_present key: :admin do
      require_relative '../generator/soql_enums'
      LeapSalesforce::Generator::SoqlEnums.new.create
    end
  end

  desc 'Create Soql Data objects'
  task :create_soql_objects do
    LeapSalesforce::Users.execute_as_if_present key: :admin do
      require_relative '../generator/soql_objects'
      LeapSalesforce::Generator::SoqlObjects.new.create_all
    end
  end

  desc 'Create objects, fields, enums'
  task create_all: :create_soql_objects do
    # TODO: This is not ideal. But only way I could get it to work
    puts `bundle exec rake leaps:create_soql_objects`
    puts 'Creating enums. Please wait until finished for output'
    puts `bundle exec rake leaps:create_enums`
  end
end
