# frozen_string_literal: true

# Convenience tasks that use sfdx according to common environment variables
namespace :sfdx do
  def login
    puts 'Note these tasks are a work in progress'
    puts `sfdx force:auth:jwt:grant --client-id $SF_CONSUMER_KEY --jwt-key-file "$JWT_FOLDER"/server.key --username $SF_USERNAME --set-default-dev-hub --alias HubOrg`
  end

  desc 'Login with credentials'
  task(:login) { login }

  desc 'Display details about user'
  task :display do
    login
    puts `sfdx force:org:display --target-org samuel.garratt@brave-otter-ttxype.com`
  end

  desc 'Create dev environment'
  task :create_dev do
    login
    puts `sfdx force:org:create --targetdevhubusername HubOrg --setdefaultusername --definitionfile config/project-scratch-def.json --alias $SCRATCH_ORG_ALIAS --wait 10 --durationdays 7`
  end

  desc 'Open environment in browser'
  task :open do
    login
    puts `sfdx force:org:open -u $SF_USERNAME`
  end

  desc 'Delete dev environment'
  task :delete_dev do
    login
    puts `sfdx force:org:delete --target-org $SCRATCH_ORG_ALIAS --noprompt`
  end
end
