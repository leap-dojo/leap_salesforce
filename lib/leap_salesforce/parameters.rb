# frozen_string_literal: true

require_relative 'users/users'
require_relative 'soql_object'
require 'logger'
require_relative 'auth'
# @return [String] Tutorial on creating an sfdx app
CREATE_APP_LINK = 'https://trailhead.salesforce.com/content/learn/projects/automate-cicd-with-gitlab/enable-dev-hub-and-create-a-connected-app'

# Adding parameters to set for authentication, environment and other common settings
module LeapSalesforce
  @config_folder = 'config'
  @lib_folder = File.join('lib', 'leap_salesforce').freeze
  @enum_folder = nil
  @objects_to_verify = []
  @soql_objects = []
  @client_id = ENV['client_id']
  @client_secret = ENV['client_secret']
  @password = ENV['password']
  @language = 'ruby'
  @soql_field_start_text = ''
  @soql_enum_start_text = ''
  @environment = nil
  @logger = Logger.new $stdout
  @sfdx = false
  # @access_token = nil
  @instance_url = nil
  # @return [String] Folder where credentials are stored
  CREDENTIALS_FOLDER = File.join('config', 'credentials')
  # @return [String] File where Salesforce credentials are stored
  CREDENTIAL_FILE = File.join(CREDENTIALS_FOLDER, 'salesforce_oauth2.yml')
  class << self
    # @return [String] Environment to use for tests. This can be accessed to change the username used to login
    #   to test.salesforce with. This can be set on the command line with 'LEAP_ENV'
    def environment
      ENV['LEAP_ENV'] || @environment
    end

    # Verify connection to Salesforce environment
    def salesforce_reachable?
      RestClient.get(LeapSalesforce.general_url)
    rescue SocketError
      message = "Unable to connect to #{LeapSalesforce.general_url}. Potentially problem with" \
      ' internet or proxy settings'.colorize :red
      raise LeapSalesforce::SetupError, message
    end

    # @return [TrueClass] If OAuth authentication is working, return true.
    #   Otherwise raise exception
    def oauth_working?
      salesforce_reachable?
      if LeapSalesforce.sfdx
        sfdx_auth_setup?
      else
        Soaspec::OAuth2.debug_oauth = true
        Soaspec::OAuth2.new(LeapSalesforce.oauth_settings).access_token
      end
    rescue StandardError => e
      raise LeapSalesforce::SetupError, "Cannot perform OAuth. See 'logs'" \
      ' folder for details of what was sent. ' \
      "Error caused by #{e.message} from #{e.backtrace}"
    else
      puts "\u2713 OAuth successful".colorize :green
      Soaspec::OAuth2.debug_oauth = false
      true
    end

    # Checks whether sfdx is setup according to standard approach. Errors are
    # logged
    # @return [Boolean] Whether sfdx is setup correctly
    def sfdx_auth_setup?
      Auth.manually_set_auth? || Auth.jwt_file?
      return true if LeapSalesforce::Auth.sfdx_variables?

      raise LeapSalesforce::SetupError, 'LeapSalesforce::Auth.access_token and ' \
           'instance_url were not able to be retrieved by sfdx'
    end

    # OAuth parameters when using a custom Connected application not
    # using sfdx
    # @return [Hash] OAuth2 parameters used in connecting to salesforce
    def oauth_settings
      {
        username: '<%= LeapSalesforce.api_user %>',
        password: '<%= LeapSalesforce.password + LeapSalesforce.security_token.to_s %>',
        client_id: LeapSalesforce.client_id,
        client_secret: LeapSalesforce.client_secret,
        token_url: "#{LeapSalesforce.general_url}/services/oauth2/token"
      }
    end

    # @return [String] General salesforce URL for logging in to
    def general_url
      "https://#{LeapSalesforce.environment == 'prod' ? 'login' : 'test'}.salesforce.com"
    end

    # @param [String, Symbol, Regexp, LeapSalesforce::User] user User or email address of user
    def api_user=(user)
      @api_user = if user.is_a? String
                    user
                  else
                    LeapSalesforce::Users.where(user)&.username
                  end
      leaps_user = LeapSalesforce::Users.where username: @api_user
      LeapSalesforce.security_token = leaps_user.security_token
      Soaspec::SpecLogger.info "Using user '#{@api_user}' for API"
    end

    # @return [String] Salesforce username used to execute API tests. This can be changed during tests
    def api_user
      @api_user || LeapSalesforce::Users.list.first.username
    end

    # @return [String] Folder where all configuration is stored. By default this is 'config'
    attr_accessor :config_folder
    # @return [String] Environment to use for tests. This can be accessed to change the username used to login
    #   to test.salesforce with
    attr_writer :environment
    # @return [String] Common API password. This assumes all users use the same password
    attr_accessor :password
    # @return [String] Client id in Salesforce OAuth app
    attr_accessor :client_id
    # @return [String] Client secret in Salesforce OAuth app
    attr_accessor :client_secret
    # @return [String] Token specific to a user used for authentication
    attr_accessor :security_token
    # @return [String] Path where library generated assets are stored. By default lib/leap_salesforce
    attr_accessor :lib_folder
    # @return [String] Path where generated enum files are stored. By default this is null and just appeneded to lib_folder
    attr_accessor :enum_folder
    # @return [Array] List of objects to verify metadata for. This includes enums, required values
    #   Changes to these values will need to be version controlled
    attr_accessor :objects_to_verify
    # @return [Array] List of Soql Objects identified for Leap Salesforce to maintain
    attr_reader :soql_objects
    # @return [Logger] Logger used by LeapSalesforce
    attr_accessor :logger

    # @return [Symbol] Name of the language used
    attr_accessor :language

    # @return [String] Text to be added as start of auto generated Soql field names
    # classes in the Salesforce object
    attr_accessor :soql_field_start_text

    # @return [String] Text to be added as start of auto generated Soql enums
    attr_accessor :soql_enum_start_text

    # @return [Array] list_of_soql_objects Array describing Soql objects taken from .leap_salesforce.yml
    def soql_objects=(list_of_soql_objects)
      @soql_objects = list_of_soql_objects.collect do |soql_object_desc|
        SoqlObject.new(soql_object_desc)
      end
    end
    # Setting this variable uses sfdx for authentication and other actions such
    # as opening an organisation link
    # @return [Boolean] Whether Salesforce sfdx is being used
    attr_accessor :sfdx
  end
end
