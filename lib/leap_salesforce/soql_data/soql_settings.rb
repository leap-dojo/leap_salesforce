# frozen_string_literal: true

# Settings for configuring usage of Soql Objects
module SoqlSettings
  # Set name of soql object. This is equivalent to the name in https://workbench.developerforce.com or
  # the lightning URL when looking at an object
  # @param [String] name Name of Soql object
  def soql_object(name)
    @soql_object_name = name
  end

  # @return [String] Name of Soql object
  def soql_object_name
    @soql_object_name || to_s
  end
end
