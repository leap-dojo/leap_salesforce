# frozen_string_literal: true

module LeapSalesforce
  # Methods to interact with tooling API.
  # See https://developer.salesforce.com/docs/atlas.en-us.api_tooling.meta/api_tooling/intro_rest_resources.htm
  module Tooling
    def tooling_objects
      new('Tooling sobjects',
          method: :get, suburl: 'tooling/sobjects')
    end

    def run_test_asynchronous
      new('Test run async', method: :post,
                            suburl: 'tooling/runTestsAsynchronous/')
    end
  end
end
