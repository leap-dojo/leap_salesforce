# frozen_string_literal: true

require 'soaspec'

# Handles basic Soql interactions. These are done through classes that are tied to this handler, i.e, the SoqlData
# class.
# This is a service that handles all interactions with the API
# Credentials are stored either in 'config/credentials' folder or via environment variables
# To check out SOQL SCHEMA go to workbench.developerforce.com. Use Sandbox and login
class SoqlHandler < Soaspec::RestHandler
  if LeapSalesforce.sfdx
    # @return [String] Instance Url taken from sfdx
    def instance_url
      LeapSalesforce::Auth.instance_url
    end

    # @return [String] Access token taken from sfdx
    def access_token
      LeapSalesforce::Auth.access_token
    end
  else
    oauth2 LeapSalesforce.oauth_settings
  end
  base_url '<%= instance_url %>/services/data/v<%= api_version %>/'

  pascal_keys true
  headers authorization: 'Bearer <%= access_token %>', content_type: 'application/json'

  element :success, '$..success'
  element :error_message_element, '$..message'
  element :soql_fields, :fields

  # This error appears to happen intermittently on test environments
  retry_on_exceptions [RestClient::ServerBrokeConnection], pause: 1, count: 5

  # @return [String] Version of Salesforce API to use
  @api_version = '61.0'

  class << self
    # @return [String] The current Salesforce instance URL obtained from oauth call
    def instance_url
      new.instance_url # Instance url is defined through oauth2_file method
    end

    # @return [String] Version of Salesforce API to use
    attr_accessor :api_version
  end

  # @return [String] Version of Salesforce API to use
  def api_version
    SoqlHandler.api_version
  end
end
