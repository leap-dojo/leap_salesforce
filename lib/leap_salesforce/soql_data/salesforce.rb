# frozen_string_literal: true

require_relative 'soql_global_data'

# ApiService that interacts with Salesforce via the API globally.
# For interacts that are specific to an object, use a class inheriting from SoqlData
# See SoqlGlobalData for details on how it's used
module Salesforce
  extend SoqlGlobalData
end
