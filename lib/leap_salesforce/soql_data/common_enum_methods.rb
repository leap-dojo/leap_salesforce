# frozen_string_literal: true

module LeapSalesforce
  # Methods common to a enum class
  module CommonEnumMethods
    # @return [String] Sample value from Enum
    def sample
      values.sample
    end

    # @return [Array] List of values for enumeration/picklist
    def values
      instance_variables.collect { |var| instance_variable_get(var) }
    end
  end
end
