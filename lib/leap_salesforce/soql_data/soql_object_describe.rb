# frozen_string_literal: true

# Methods relating to describing a soql object
# See https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/resources_sobject_describe.htm
# for reference.
# Note the data here is cached. If metadata changes you would need to re run Ruby Code to get updates if a call has
# already been made
module SoqlObjectDescribe
  # Reference https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/sobject_describe_with_ifmodified_header.htm
  # @todo Get this to work
  def changes_from_date(_date)
    @changes_from_date ||= new("describe #{self}", method: :get, suburl: "sobjects/#{soql_object_name}/describe/",
                                                   params: { if_modified_since: 'Wed, 3 Jul 2013 19:43:31 GMT' })
  end

  # @return [SoqlData] Retrieve JSON that describes current object
  def description
    @description ||= new("describe #{self}", method: :get, suburl: "sobjects/#{soql_object_name}/describe/")
  end

  def layouts
    @layouts ||= new("layouts for #{self}",
                     method: :get, suburl: "sobjects/#{soql_object_name}/describe/layouts")
  end

  def layouts_for(record_type_id)
    new("layouts for #{self} on #{record_type_id}",
        method: :get, suburl: "sobjects/#{soql_object_name}/describe/layouts/#{record_type_id}")
  end

  # @return [Array] List of fields that each are a hash of attributes
  def fields
    @fields ||= description[:fields]
  end

  # @param [String, Symbol] field_name Salesforce backend field name
  # @return [Hash] Hash storing all properties of a field
  def properties_for(field_name)
    field_name = field_name.to_s
    properties = fields.find { |field| %w[name label].any? { |label| field[label] == field_name } }
    unless properties
      raise LeapSalesforce::ResponseError, "Field name '#{field_name}' not found in '#{self}'" \
      " using table name '#{soql_object_name}'. Field names are #{field_names}"
    end

    properties
  end

  # Finding Picklist values for specified fields
  # @param [String, Symbol] field_name Salesforce backend field name
  # @return [Array] List of values for passed in field_name
  def picklist_for(field_name)
    properties = properties_for field_name

    properties['picklistValues'].collect { |list| list['label'] }.compact
  end

  # @param [String, Symbol] field_name Salesforce backend field name
  # @return [Integer] Max length of field
  def length_for(field_name)
    properties_for(field_name)['length']
  end

  # @param [String, Symbol] field_name Salesforce backend field name
  # @return [String, nil] Default value for field provided
  def default_for(field_name)
    properties_for(field_name)['defaultValue']
  end

  def required
    fields.find_all { |field| field[''] }
  end

  # @return [Array] List of labels that have picklists
  def picklists
    label_names.find_all { |f| type_for(f) == 'picklist' }
  end

  # @param [String, Symbol] field_name Salesforce backend field name
  # @return [String] Type of field (e.g., picklist, string, double, reference)
  def type_for(field_name)
    properties_for(field_name)['type']
  end

  # @param [String, Symbol] field_name Salesforce backend field name
  # @return [String, nil] Other entity this field relates to if it's a reference field
  def relationship_name_for(field_name)
    properties_for(field_name)['relationshipName']
  end

  # @return [Array] Label names of object
  def label_names
    fields.collect { |field| field['label'] }
  end

  # @return [Array]
  # Field values for field names
  def field_names
    fields.collect { |field| field['name'] }
  end

  # @return [Hash] List of accessors for an object and what they relate to
  def accessors
    return @attr_hash if @attr_hash

    @attr_hash = {}
    fields.each do |field|
      @attr_hash[field['label'].unused_ruby_name.to_sym] = important_attributes_for(field)
    end
    @attr_hash
  end

  private

  # @return [Hash] Important attributes for a field
  def important_attributes_for(field)
    important_attributes = { backend: field['name'],
                             label: field['label'],
                             type: field['type'] }
    relationship_name = field['relationshipName']
    important_attributes[:related_object] = relationship_name if relationship_name
    important_attributes
  end
end
