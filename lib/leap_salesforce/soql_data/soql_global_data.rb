# frozen_string_literal: true

# Global Methods to interact with Soql Data
module SoqlGlobalData
  attr_accessor :ids_to_delete

  # Return limits of Salesforce org. https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/resources_limits.htm
  # @example See Conga limits
  #   SoqlData.limits["['Conga Composer - EU']"]
  # @return [Exchange] Limits of Salesforce ORG
  def limits
    @limits ||= SoqlData.new("#{self} limits", method: :get, suburl: 'limits/')
  end

  # Describe all salesforce objects (this returns BIG response)
  # See https://developer.salesforce.com/docs/atlas.en-us.api_rest.meta/api_rest/resources_describeGlobal.htm for reference
  # @example Count how many sobjects there are
  #   objects = SoqlData.global_describe['sobjects']
  #   objects.size
  # @example Find description of object with label Organisation
  #   org_desc = SoqlData.global_describe['sobjects'].find { |obj| obj['label'] == 'Organisation' }
  # @example Find names of all objects that include 'File' in their label
  #   SoqlData.global_describe['sobjects'].find_all { |obj| obj['label'].include? 'File' }.collect { |f| f['name'] }
  # @return [Exchange] Global describe of salesforce Objects
  def global_describe
    @global_describe ||= SoqlData.new('Global describe of sobjects', method: :get, suburl: 'sobjects/')
  end
end
