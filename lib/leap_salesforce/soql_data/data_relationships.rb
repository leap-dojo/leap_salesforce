# frozen_string_literal: true

# Represents common relationships from a Data class
module DataRelationships
  # @example
  #   # Retrieve organisation record associated to an opportunity and then get its name
  #   Opportunity.organisation.name
  # Retrieve organisation related to current opportunity
  def organisation
    raise '"Organisation" class not yet defined' unless defined? Organisation

    Organisation.find(Id: self['AccountId'])
  end

  # @example Get user name
  #   record.owner.name
  # @return [Exchange] object representing owner of object
  def owner
    User.find(Id: self[:owner_id])
  end

  # @example Get user name
  #   record.queue.name
  # @return [Exchange] object representing owner of object
  def queue
    Group.find(Id: self[:owner_id])
  end

  # Retrieve record type for current object
  def record_type
    raise '"RecordType" class not yet defined' unless defined? RecordType

    RecordType.find(Id: self['RecordTypeId'])
  end
end
