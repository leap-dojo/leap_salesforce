# frozen_string_literal: true

# Handles tests interaction and verification based upon Metadata
class MetaDataHandler
  @objects_to_verify = []
  class << self
    # @return [Array] List of objects to verify metadata for. This includes enums, required values
    #   Changes to these values will need to be version controlled
    attr_accessor :objects_to_verify
  end
end
