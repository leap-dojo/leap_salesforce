# frozen_string_literal: true

require_relative 'common_enum_methods'
# Module all SoqlEnums include
module SoqlEnum
  # @param [Class] object Object inheriting from SoqlData that has picklists underneath it
  # @return [Array] List of picklist classes that inherit from SoqlEnum and relate to provided class
  def self.values_for(object)
    ObjectSpace.each_object(Module).select { |class_name| class_name < self && class_name.to_s.start_with?("#{object}::") }
  end
end
