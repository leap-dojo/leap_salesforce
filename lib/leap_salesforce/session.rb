# frozen_string_literal: true

require_relative 'auth'
require 'savon'
require 'json'

module LeapSalesforce
  # Holds information about a login session
  class Session
    # @return [String] Session id returned from SOAP API
    attr_accessor :session_id
    # @return [String] User id returned from SOAP API
    attr_accessor :user_id
    # @return [Hash] Login response
    attr_accessor :login_response

    def initialize(username, password, security_token = '')
      login_body = LeapSalesforce::Session.soap_login username, password, security_token

      self.session_id = login_body[:login_response][:result][:session_id]
      self.user_id = login_body[:login_response][:result][:user_id]
      self.login_response = login_body[:login_response]
    end

    class << self
      # Login via SOAP API
      def soap_login(username, password, security_token)
        client = Savon.client do
          endpoint "#{SoqlHandler.instance_url}/services/Soap/u/51.0"
          namespace 'urn:partner.soap.sforce.com'
          log true # See request and response. (Put this in traffic file)
          log_level :debug
          logger Soaspec::SpecLogger.create
          pretty_print_xml true # Prints XML pretty
        end

        response = client.call(:login, message:
            {
              username: username,
              password: password + security_token.to_s
            })
        response.body
      end
    end
  end
end
