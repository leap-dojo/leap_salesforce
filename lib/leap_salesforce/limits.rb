# frozen_string_literal: true

module LeapSalesforce
  # Handles retrieving limits of a Salesforce instance
  class Limits
    class << self
      # @param [Symbol, String] limit_name Name of limit to find remaining amount for
      # @return [Integer] Finds remaining amount for a Salesforce limit
      def remaining_for(limit_name)
        Salesforce.limits[limit_name]['Remaining']
      end
    end
  end
end
