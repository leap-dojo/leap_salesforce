# frozen_string_literal: true

module LeapSalesforce
  # @return [String] Version of leap salesforce
  VERSION = '1.5.0'
end
