# frozen_string_literal: true

module LeapSalesforce
  # Container for information regarding a SoqlObject
  class SoqlObject
    # @return [String] Ruby class name
    attr_accessor :class_name
    # @return [String] Salesforce backend name
    attr_accessor :backend_name
    # @return [String] Snakecase version of ruby class. Name for methods / files to refer to class as
    attr_accessor :reference
    # @return [String] Description of Soql object from '.leap_salesforce' YAML
    attr_accessor :description
    # @return [Boolean, Hash] Whether to create enumerations for object
    attr_accessor :create_enum

    # Create a representation of a Soql object from a description (usually in .leap_salesforce.yml)
    # @example Specific Salesforce object 'Case' to be represented
    #   'Case'
    # @example Represent Broker object by class with different name
    #   { 'Broker' => Broker__c }
    # @example Represent Group object, specifying to ignore enums
    #   {"Group"=>nil, "create_enum"=>true} }
    # @example Represent User object, specifying to ignore enum with name of Timezone
    #   {"User"=>nil, "create_enum"=>{"exclude"=>["Timezone"]}}
    # @param [Hash, String] description
    def initialize(description)
      self.description = description
      interpret_description
      self.reference = @class_name.snakecase
    end

    # @param [String] picklist
    # @return [Boolean] Whether picklist should be excluded from being generated
    def excludes?(picklist)
      if create_enum.is_a? Hash
        if create_enum['exclude']
          picklist_class = picklist.to_class_name
          create_enum['exclude'].any? do |exclusion_list|
            !picklist.to_s[Regexp.new(exclusion_list)].nil? || !picklist_class[Regexp.new(exclusion_list)].nil?
          end
        else
          false
        end
      else
        false
      end
    end

    private

    # Set attributes based on description
    def interpret_description
      if description.is_a? Hash
        interpret_hash_description
      else
        self.backend_name = description.to_s
        self.class_name = backend_name.tr(" '/()-", '').tr('ā', 'a')
        self.create_enum = true
      end
    end

    # Set attributes based on description where description is a Hash
    def interpret_hash_description
      self.create_enum = create_enum?
      self.class_name = description.keys[0]
      self.backend_name = description.values[0] || description.keys[0]
    end

    # @return [Boolean] Whether to create enum. Default is true.
    def create_enum?
      description['create_enum'].nil? ? true : description['create_enum']
    end
  end
end
