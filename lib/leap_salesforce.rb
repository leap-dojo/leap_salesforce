# frozen_string_literal: true

require 'leap_salesforce/version'
require 'facets/string/snakecase'
require 'soaspec'
Soaspec::OAuth2.refresh_token = :once   # Save access token and reuse it
Soaspec.log_warnings = false            # Log any API warnings
Soaspec::SpecLogger.progname = 'Leaps'  # Program name shown in logs
Soaspec::OAuth2.request_message = false # Don't display message explaining OAuth request
Soaspec::OAuth2.retry_limit = 1 # Retrying for OAuth token too many times results in user locked out
require 'active_support/isolated_execution_state'
require 'active_support/core_ext/integer/time' # Creating time objects
require 'leap_salesforce/parameters' # Important to load this first
require 'leap_salesforce/ext/string'
require 'leap_salesforce/ext/time'
require 'require_all'
require 'rake'
require 'factory_bot' # For mass production of data
require 'faker' # For fake data
require 'leap_salesforce/error'
require 'leap_salesforce/loader'
require 'leap_salesforce/session'

# If variable is present within LeapSalesforce set it
def load_variables_from(file)
  YAML.load_file(file).each do |key, value|
    if respond_to? "#{key}="
      if !send(key).nil?
        logger.info "Ignoring #{key} from #{file} as already set"
      else
        send("#{key}=", value)
      end
    else
      logger.info "LeapSalesforce ignoring unknown attribute '#{key}'"
    end
  end
end

# Global configuration for LeapSalesforce
module LeapSalesforce
  logger.level = Logger::DEBUG
  logger.datetime_format = '%Y-%m-%d %H:%M:%S'

  LeapSalesforce::Loader.load_config_file
  raise SetupError, 'LeapSalesforce.environment not set' if environment.nil? && !sfdx

  # Set credentials from credentials file for ConnectedApp
  load_variables_from CREDENTIAL_FILE if File.exist? CREDENTIAL_FILE

  # Either SF_CONSUMER_KEY or client_id can be set
  ENV['SF_CONSUMER_KEY'] ||= client_id
  self.client_id ||= ENV['SF_CONSUMER_KEY']

  config_folder_path = File.join(Dir.pwd, config_folder)
  general_file = File.join(config_folder_path, 'general')
  if environment
    specific_environment_file = File.join(config_folder_path, 'environments',
                                          environment)
    require specific_environment_file if File.exist? "#{specific_environment_file}.rb"
  end
  require general_file if File.exist? "#{general_file}.rb"

  if sfdx
    sfdx_auth_setup?
  else
    %w[client_id client_secret password].each do |param|
      if send(param).nil?
        logger.error "No CREDENTIALS FILE found at #{CREDENTIAL_FILE}." unless File.exist? CREDENTIAL_FILE
        raise SetupError, "LeapSalesforce.#{param} not set"
      end
    end
  end

  require 'leap_salesforce/soql_data/soql_data'
  require 'leap_salesforce/soql_data/salesforce'
  require 'leap_salesforce/limits'

  FileUtils.mkdir_p lib_folder unless Dir.exist? lib_folder
  %w[factories metadata soql_data].each do |folder|
    sub_folder = File.join(lib_folder, folder)
    require_all sub_folder if Dir.exist? sub_folder
  end

  self.objects_to_verify = SoqlData.descendants if objects_to_verify.empty?
end

LeapSalesforce.api_user = LeapSalesforce::Users.list.first.username
