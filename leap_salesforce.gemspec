# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'leap_salesforce/version'

Gem::Specification.new do |spec|
  spec.name          = 'leap_salesforce'
  spec.version       = LeapSalesforce::VERSION
  spec.authors       = ['IQA', 'Samuel Garratt']
  spec.email         = %w[samuel.garratt@integrationqa.com leap.salesforce@integrationqa.com]

  spec.summary       = 'Helps with setting up integrated automated test frameworks
                        against Salesforce.'
  spec.description   = 'This gem helps ones to perform integration tests on Salesforce.
It reads the Metadata from Salesforce and creates the foundation for API tests.'
  spec.homepage      = 'https://gitlab.com/leap-dojo/leap_salesforce'
  spec.license       = 'MIT'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(spec)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'parallel'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'semaphore_test_boosters'
  spec.add_development_dependency 'topoisomerase'
  spec.add_development_dependency 'yard-doctest'
  spec.add_dependency 'activerecord', '7'
  spec.add_dependency 'colorize'
  spec.add_dependency 'dotenv'
  spec.add_dependency 'facets'
  spec.add_dependency 'factory_bot'
  spec.add_dependency 'faker', '>= 2.0'
  spec.add_dependency 'humanize'
  spec.add_dependency 'launchy'
  spec.add_dependency 'rake'
  spec.add_dependency 'require_all'
  spec.add_dependency 'rubocop'
  spec.add_dependency 'simplecov'
  spec.add_dependency 'soaspec', '>= 0.3.10' # Version can send payload for :delete method
  spec.add_dependency 'thor'
end
