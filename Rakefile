# frozen_string_literal: true

require_relative 'config/credentials/token' if File.exist? 'config/credentials/token.rb'
require 'bundler/gem_tasks'
require 'rspec/core/rake_task'
require 'rake/clean'
require 'yard'
require 'yard/doctest/rake'

require 'dotenv'
Dotenv.load
require 'leap_salesforce/rake' # TODO: Append this line to existing repo

CLEAN.include 'tmp/*'

RSpec::Core::RakeTask.new(:spec)

desc 'Check Salesforce OAuth2 authentication is working'
task :check_oauth do
  Soaspec::OAuth2.debug_oauth = true
  LeapSalesforce.oauth_working?
end

task default: %i[clean check_oauth spec]

require 'leap_salesforce'

desc "Create stubs for dynamic methods so they're picked up by the IDE"
task :stub do
  require 'topoisomerase'
  Topoisomerase.create_stubs_based_on SoqlData
end

desc 'Test examples shown in documentation'
YARD::Doctest::RakeTask.new do |task|
  task.doctest_opts = %w[-v]
  # task.pattern = 'lib/**/*.rb'
  # TODO: Get all examples working in this way
  task.pattern = 'lib/leap_salesforce/ext/string.rb'
end

desc 'Task to enter pry terminal and types commands using the standard library code'
task :pry do
  require 'pry'
  Pry.start
end

desc 'Example of cleaning up data'
task :remove_contacts do
  require 'parallel'
  contacts = Contact.where first_name: '!null'
  puts "Deleting #{contacts.size} contacts"
  Parallel.each(contacts) do |contact|
    contact.delete
    print '.'
  end
  contacts = Contact.where first_name: '!null'
  puts "#{contacts.size} contacts left"
end
