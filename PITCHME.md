# Leap Salesforce

Making Salesforce testing easy

---

## Contents

* What does this library do
* Bootstrap of test automation suite from Salesforce credentials
* Verification of Salesforce on different levels
* Creation of objects
* Querying for objects
* Updating objects
* Deleting objects

---

## What it does

Makes it easy to create integrated tests using Salesforce API with Ruby objects that map to objects in Salesforce. 
Ruby gem is open source and has many convenience methods to performing actions that form automated tests.

---

## Bootstrapping test automation suite

* Install gem
* From Salesforce credentials create a full automation suite
* See a recording of running through this [here](https://asciinema.org/a/259098)

---

## Verification on different levels

From faster with broadest coverage to slower and harder to maintain 

* Metadata
* System
* Integration  
* UI

---

## Metadata

Creating a baseline of Metadata and checking what has changed (For example, the values in a picklist can be 
recorded and then every time there's a change, the test will be to whether the values are the same. The baseline
can be re-recorded easily when a change is meant to occur). 

---

## System

Entities can have single actions on them and a small step checked. (For example, not setting all mandatory fields
can be checked to see if an error message occurs or an update could be made to an object that automatically creates
another object which can be verified)

---

## Integration

Business processes that verify the integration of entities, automated processes, third party systems, etc

---

## UI

Crucial actions can be performed via the UI to verify the User interface.
At the moment this library does not assist in UI testing but the plan is
to accommodate this in the future through page objects that are tied to
metadata.

---

## Creation of data

---?code=spec/integration/creating_spec.rb&lang=ruby&title=Creating entities

@[3](Declare what it is you're testing)
@[5](Define what 'it' is, a the object under test must do)
@[6](Create an instance of the object class - nothing sent to Salesforce yet)
@[4,7-8](Set the first and last name to random names)
@[9](Create the object in Salesforce by sending request via API)
@[10](Verify creation was successful)
@[11](Verify first name set as expected)
@[19-22](Creating through Factory)

---

## Reading data

---?code=spec/integration/query_spec.rb&lang=ruby&title=Querying entities

@[21](Find an object using a specific attribute specific value)
@[29-30](Find list of objects meeting condition)

---

## Updating and deleting data

---?code=spec/integration/crud_eg_spec.rb&lang=ruby&title=Updating entities

@[8-9,20-21](Update first name using 'first_name' setting)
@[29](Delete test contact)

---

## Thank you

Any questions?
