# frozen_string_literal: true

require_relative '../config/credentials/token' if File.exist? 'config/credentials/token.rb'
require 'bundler/setup'
require 'simplecov'
SimpleCov.start
SimpleCov.start do
  add_filter %r{^/spec/}
end
require 'leap_salesforce/leaps'
require 'faker'

# Default user is the first one set
# LeapSalesforce.api_user = LeapSalesforce::Users.where(key: :cet)
LeapSalesforce.api_user = LeapSalesforce::Users.where(key: :admin)

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end

require 'open3' # Used for handling stdout
# Makes accessing results of commands via command line easy
class CmdLine
  # Easy access to status of command line run
  attr_reader :stdout, :stderr, :status

  # @param [String] command_line_to_run Command to run on terminal
  # @param [String] options Open3 options, E.g, chdir to execute within a folder
  def initialize(command_line_to_run, options)
    @name = command_line_to_run
    @stdout, @stderr, @status = Open3.capture3(command_line_to_run, options)
  end

  # @return [String] Result of executing Terminal instruction with STDOUT, STDERR, and Status
  def result
    stdout + stderr + status.to_s
  end

  # Return name of command line typed
  def to_s
    @name
  end
end
