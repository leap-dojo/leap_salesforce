# frozen_string_literal: true

RSpec.describe 'leap_salesforce init' do
  before(:all) do
    FileUtils.mkdir_p 'tmp/init/no_sfdx'
    FileUtils.mkdir_p 'tmp/init/sfdx/assets'
    FileUtils.mkdir_p 'tmp/init/sfdx/JWT'
    FileUtils.cp 'assets/server.key', 'tmp/init/sfdx/JWT/'
    FileUtils.cp 'assets/server.key', 'tmp/init/sfdx/assets/'
  end
  let(:params) do
    {
      setup_done: true,
      client_id: LeapSalesforce.client_id,
      client_secret: LeapSalesforce.client_secret,
      username: LeapSalesforce.api_user,
      user_key: :admin,
      password: LeapSalesforce.password,
      environment: LeapSalesforce.environment,
      sfdx: false
    }
  end
  let(:sfdx_params) do
    {
      setup_done: true,
      sfdx: true,
      client_id: LeapSalesforce.client_id,
      username: LeapSalesforce.api_user,
      user_key: :admin # Not sure if this is really necessary for sfdx
    }
  end
  let(:expand_params) do
    expanded = ''
    params.each { |key, value| expanded += "--#{key}=#{value} " }
    expanded
  end
  let(:expand_sfdx_params) do
    expanded = ''
    sfdx_params.each { |key, value| expanded += "--#{key}=#{value} " }
    expanded
  end
  let(:init_leap) do
    CmdLine.new("ruby ../../../exe/leap_salesforce init #{expand_params}", chdir: 'tmp/init/no_sfdx')
  end
  let(:init_leap_sfdx) do
    CmdLine.new("ruby ../../../exe/leap_salesforce init #{expand_sfdx_params}", chdir: 'tmp/init/sfdx')
  end
  it 'No sfdx Creates files and runs tests' do
    puts init_leap.result
    expect(init_leap.result).to include 'config/general.rb'
    expect(init_leap.status).to eq 0
    # Run tests after creation
    run_tests = CmdLine.new('rake', chdir: 'tmp/init/no_sfdx')
    puts run_tests.result
    expect(run_tests.status).to eq 0
  end
  it 'Sfdx Creates files and runs tests' do
    puts init_leap_sfdx.result
    expect(init_leap_sfdx.result).to include 'config/general.rb'
    expect(init_leap_sfdx.status).to eq 0
    # Run tests after creation
    run_tests = CmdLine.new('rake', chdir: 'tmp/init/sfdx')
    puts run_tests.result
    expect(run_tests.status).to eq 0
  end
end
