# frozen_string_literal: true

RSpec.describe 'leap_salesforce version' do
  let(:get_version) do
    CmdLine.new('ruby exe/leap_salesforce version', chdir: '.')
  end
  it 'Shows the version' do
    expect(get_version.result).to include LeapSalesforce::VERSION
    expect(get_version.status).to eq 0
  end
end
