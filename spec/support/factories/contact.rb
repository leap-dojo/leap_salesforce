# frozen_string_literal: true

# Basic factory settings made according to values defined at
# https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_objects_case.htm
FactoryBot.define do
  factory :contact do
    last_name { Faker::Lorem.paragraph_by_chars(number: 80) }
    first_name { Faker::Lorem.paragraph_by_chars(number: 40) }
    trait :account do
      association :account_id, factory: :account
    end
    trait :coordinates do
      location_longitude { '2.0' }
      location_latitude { '2.0' }
    end
    trait :no_last_name do
      unset { :LastName }
    end
    trait :all do
      contact_id { 'Best to not hard code this' }
      deleted { true }
      # Please add MasterRecord to .leap_salesforce.yml (if it's a table) to create association for MasterRecord
      salutation { Contact::Salutation.sample }
      full_name { Faker::Lorem.paragraph_by_chars(121) }
      other_street { 'Content depending on textarea' }
      other_city { Faker::Lorem.paragraph_by_chars(40) }
      other_state_province { Faker::Lorem.paragraph_by_chars(80) }
      other_zip_postal_code { Faker::Lorem.paragraph_by_chars(20) }
      other_country { Faker::Lorem.paragraph_by_chars(80) }
      other_latitude { 'Content depending on double' }
      other_longitude { 'Content depending on double' }
      other_geocode_accuracy { Contact::OtherGeocodeAccuracy.sample }
      other_address { 'Content depending on address' }
      mailing_street { 'Content depending on textarea' }
      mailing_city { Faker::Lorem.paragraph_by_chars(40) }
      mailing_state_province { Faker::Lorem.paragraph_by_chars(80) }
      mailing_zip_postal_code { Faker::Lorem.paragraph_by_chars(20) }
      mailing_country { Faker::Lorem.paragraph_by_chars(80) }
      mailing_latitude { 'Content depending on double' }
      mailing_longitude { 'Content depending on double' }
      mailing_geocode_accuracy { Contact::MailingGeocodeAccuracy.sample }
      mailing_address { 'Content depending on address' }
      business_phone { 'Content depending on phone' }
      business_fax { 'Content depending on phone' }
      mobile_phone { 'Content depending on phone' }
      home_phone { 'Content depending on phone' }
      other_phone { 'Content depending on phone' }
      asst_phone { 'Content depending on phone' }
      # Please add ReportsTo to .leap_salesforce.yml (if it's a table) to create association for ReportsTo
      email { 'Content depending on email' }
      title { Faker::Lorem.paragraph_by_chars(128) }
      department { Faker::Lorem.paragraph_by_chars(80) }
      assistants_name { Faker::Lorem.paragraph_by_chars(40) }
      lead_source { Contact::LeadSource.sample }
      birthdate { 'Content depending on date' }
      contact_description { 'Content depending on textarea' }
      owner_id { User.find(CreatedDate: "<#{0.days.ago}").id }
      created_date { 'Content depending on datetime' }
      # Please add CreatedBy to .leap_salesforce.yml (if it's a table) to create association for CreatedBy
      last_modified_date { 'Content depending on datetime' }
      # Please add LastModifiedBy to .leap_salesforce.yml (if it's a table) to create association for LastModifiedBy
      system_modstamp { 'Content depending on datetime' }
      last_activity { 'Content depending on date' }
      last_stayin_touch_request_date { 'Content depending on datetime' }
      last_stayin_touch_save_date { 'Content depending on datetime' }
      last_viewed_date { 'Content depending on datetime' }
      last_referenced_date { 'Content depending on datetime' }
      email_bounced_reason { Faker::Lorem.paragraph_by_chars(255) }
      email_bounced_date { 'Content depending on datetime' }
      is_email_bounced { true }
      photo_url { 'Content depending on url' }
      datacom_key { Faker::Lorem.paragraph_by_chars(20) }
      jigsaw_contact_id { Faker::Lorem.paragraph_by_chars(20) }
      clean_status { Contact::CleanStatus.sample }
      level { Contact::Level.sample }
      languages { Faker::Lorem.paragraph_by_chars(100) }
      location_latitude { 'Content depending on double' }
      location_longitude { 'Content depending on double' }
      location { 'Content depending on location' }
    end
  end
end
