# frozen_string_literal: true

require_relative 'contact_field_names'
# An Contact object mapping to a SOQL Contact
class Contact < SoqlData
  include Contact::Fields

  # Example of deleting a dependent record. In this case the related account will be deleted
  # when the contact is deleted
  def self.remove_dependent_records(id)
    account_id = Contact.find(Id: id).account_id
    Account.delete account_id if account_id
  end
end
