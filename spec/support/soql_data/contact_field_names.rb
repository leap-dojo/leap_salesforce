# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Fields for Contact mapped from SOQL Contact
class Contact < SoqlData
  # List of accessors for retrieving and setting fields in Contact
  module Fields
    extend SoqlGlobalObjectData

    # Element for 'Contact ID', type 'id'
    soql_element :contact_id, 'Id'

    # Element for 'Deleted', type 'boolean'
    soql_element :deleted, 'IsDeleted'

    # Element for 'Master Record ID', type 'reference'
    soql_element :master_record_id, 'MasterRecordId'

    # Element for 'Account ID', type 'reference'
    soql_element :account_id, 'AccountId'

    # Element for 'Last Name', type 'string'
    soql_element :last_name, 'LastName'

    # Element for 'First Name', type 'string'
    soql_element :first_name, 'FirstName'

    # Element for 'Salutation', type 'picklist'
    soql_element :salutation, 'Salutation'

    # Element for 'Full Name', type 'string'
    soql_element :full_name, 'Name'

    # Element for 'Record Type ID', type 'reference'
    soql_element :record_type_id, 'RecordTypeId'

    # Element for 'Other Street', type 'textarea'
    soql_element :other_street, 'OtherStreet'

    # Element for 'Other City', type 'string'
    soql_element :other_city, 'OtherCity'

    # Element for 'Other State/Province', type 'string'
    soql_element :other_state_province, 'OtherState'

    # Element for 'Other Zip/Postal Code', type 'string'
    soql_element :other_zip_postal_code, 'OtherPostalCode'

    # Element for 'Other Country', type 'string'
    soql_element :other_country, 'OtherCountry'

    # Element for 'Other Latitude', type 'double'
    soql_element :other_latitude, 'OtherLatitude'

    # Element for 'Other Longitude', type 'double'
    soql_element :other_longitude, 'OtherLongitude'

    # Element for 'Other Geocode Accuracy', type 'picklist'
    soql_element :other_geocode_accuracy, 'OtherGeocodeAccuracy'

    # Element for 'Other Address', type 'address'
    soql_element :other_address, 'OtherAddress'

    # Element for 'Mailing Street', type 'textarea'
    soql_element :mailing_street, 'MailingStreet'

    # Element for 'Mailing City', type 'string'
    soql_element :mailing_city, 'MailingCity'

    # Element for 'Mailing State/Province', type 'string'
    soql_element :mailing_state_province, 'MailingState'

    # Element for 'Mailing Zip/Postal Code', type 'string'
    soql_element :mailing_zip_postal_code, 'MailingPostalCode'

    # Element for 'Mailing Country', type 'string'
    soql_element :mailing_country, 'MailingCountry'

    # Element for 'Mailing Latitude', type 'double'
    soql_element :mailing_latitude, 'MailingLatitude'

    # Element for 'Mailing Longitude', type 'double'
    soql_element :mailing_longitude, 'MailingLongitude'

    # Element for 'Mailing Geocode Accuracy', type 'picklist'
    soql_element :mailing_geocode_accuracy, 'MailingGeocodeAccuracy'

    # Element for 'Mailing Address', type 'address'
    soql_element :mailing_address, 'MailingAddress'

    # Element for 'Business Phone', type 'phone'
    soql_element :business_phone, 'Phone'

    # Element for 'Business Fax', type 'phone'
    soql_element :business_fax, 'Fax'

    # Element for 'Mobile Phone', type 'phone'
    soql_element :mobile_phone, 'MobilePhone'

    # Element for 'Home Phone', type 'phone'
    soql_element :home_phone, 'HomePhone'

    # Element for 'Other Phone', type 'phone'
    soql_element :other_phone, 'OtherPhone'

    # Element for 'Asst. Phone', type 'phone'
    soql_element :asst_phone, 'AssistantPhone'

    # Element for 'Reports To ID', type 'reference'
    soql_element :reports_to_id, 'ReportsToId'

    # Element for 'Email', type 'email'
    soql_element :email, 'Email'

    # Element for 'Title', type 'string'
    soql_element :title, 'Title'

    # Element for 'Department', type 'string'
    soql_element :department, 'Department'

    # Element for 'Assistant's Name', type 'string'
    soql_element :assistants_name, 'AssistantName'

    # Element for 'Lead Source', type 'picklist'
    soql_element :lead_source, 'LeadSource'

    # Element for 'Birthdate', type 'date'
    soql_element :birthdate, 'Birthdate'

    # Element for 'Contact Description', type 'textarea'
    soql_element :contact_description, 'Description'

    # Element for 'Owner ID', type 'reference'
    soql_element :owner_id, 'OwnerId'

    # Element for 'Created Date', type 'datetime'
    soql_element :created_date, 'CreatedDate'

    # Element for 'Created By ID', type 'reference'
    soql_element :created_by_id, 'CreatedById'

    # Element for 'Last Modified Date', type 'datetime'
    soql_element :last_modified_date, 'LastModifiedDate'

    # Element for 'Last Modified By ID', type 'reference'
    soql_element :last_modified_by_id, 'LastModifiedById'

    # Element for 'System Modstamp', type 'datetime'
    soql_element :system_modstamp, 'SystemModstamp'

    # Element for 'Last Activity', type 'date'
    soql_element :last_activity, 'LastActivityDate'

    # Element for 'Last Stay-in-Touch Request Date', type 'datetime'
    soql_element :last_stayin_touch_request_date, 'LastCURequestDate'

    # Element for 'Last Stay-in-Touch Save Date', type 'datetime'
    soql_element :last_stayin_touch_save_date, 'LastCUUpdateDate'

    # Element for 'Last Viewed Date', type 'datetime'
    soql_element :last_viewed_date, 'LastViewedDate'

    # Element for 'Last Referenced Date', type 'datetime'
    soql_element :last_referenced_date, 'LastReferencedDate'

    # Element for 'Email Bounced Reason', type 'string'
    soql_element :email_bounced_reason, 'EmailBouncedReason'

    # Element for 'Email Bounced Date', type 'datetime'
    soql_element :email_bounced_date, 'EmailBouncedDate'

    # Element for 'Is Email Bounced', type 'boolean'
    soql_element :is_email_bounced, 'IsEmailBounced'

    # Element for 'Photo URL', type 'url'
    soql_element :photo_url, 'PhotoUrl'

    # Element for 'Data.com Key', type 'string'
    soql_element :datacom_key, 'Jigsaw'

    # Element for 'Jigsaw Contact ID', type 'string'
    soql_element :jigsaw_contact_id, 'JigsawContactId'

    # Element for 'Clean Status', type 'picklist'
    soql_element :clean_status, 'CleanStatus'

    # Element for 'Level', type 'picklist'
    soql_element :level, 'Level__c'

    # Element for 'Languages', type 'string'
    soql_element :languages, 'Languages__c'

    # Element for 'Location (Latitude)', type 'double'
    soql_element :location_latitude, 'Location__Latitude__s'

    # Element for 'Location (Longitude)', type 'double'
    soql_element :location_longitude, 'Location__Longitude__s'

    # Element for 'Location', type 'location'
    soql_element :location, 'Location__c'
  end
end
