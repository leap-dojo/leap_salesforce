# frozen_string_literal: true

RSpec.describe 'Updating Contact' do
  let(:new_name) { 'New name' }
  it 'can ensure that updated passes' do
    @contact = FactoryBot.create(:contact)
    @contact.success_update first_name: new_name
    expect(@contact.get[:first_name]).to eq new_name
  end
  after { @contact&.delete_as_admin }
end
