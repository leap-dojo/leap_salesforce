# frozen_string_literal: true

RSpec.describe 'Composite requests' do
  it 'can create contact and dependent account' do
    contact = FactoryBot.build :contact
    default_contact_params = contact.request_parameters.body_params

    outer_hash = { allOrNone: true, compositeRequest: [] }

    new_account_body = FactoryBot.build(:account).request_parameters.body_params
    account_reference = 'NewAccount'
    create_account = { method: 'POST', url: '/services/data/v38.0/sobjects/Account',
                       referenceId: account_reference, body: new_account_body }

    outer_hash[:compositeRequest] << create_account

    new_contact_body = default_contact_params.merge(AccountId: '@{NewAccount.id}')
    create_dependent_contact = { method: 'POST', url: '/services/data/v38.0/sobjects/Contact',
                                 referenceId: 'NewContact', body: new_contact_body }

    outer_hash[:compositeRequest] << create_dependent_contact
    payload = JSON.generate outer_hash
    # TODO: New class, SalesforceService that is not specific to SoqlData
    composite_exchange = SoqlData.new 'Composite', method: :post, suburl: 'composite/',
                                                   payload: payload
    expect(composite_exchange['$..compositeResponse[0].body.success']).to be true
    expect(composite_exchange['$..compositeResponse[1].body.success']).to be true
    account_id = composite_exchange['$..compositeResponse[0].body.id']
    contact_id = composite_exchange['$..compositeResponse[1].body.id']
    puts "Created Contact '#{contact_id}' dependent on '#{account_id}'"
    Account.delete account_id # Deletes contact as well
  end
end
