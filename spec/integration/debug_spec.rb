# frozen_string_literal: true

# Tests specific to debugging a record created

RSpec.describe 'Entity' do
  it 'can be viewed on UI' do
    @contact = Contact.create
    expect(Launchy).to receive(:open).with("#{SoqlHandler.instance_url}/#{@contact.id}")
    @contact.open_on_ui
  end
  after { @contact.delete }
end
