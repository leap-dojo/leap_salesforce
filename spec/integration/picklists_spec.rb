# frozen_string_literal: true

RSpec.describe 'Picklists', no_retry: true do
  LeapSalesforce.objects_to_verify.each do |data_class|
    SoqlEnum.values_for(data_class).each do |picklist|
      it "#{picklist} has not changed values" do
        expect(data_class.picklist_for(picklist.name)).to match_array picklist.values
      end
    end
  end
end
