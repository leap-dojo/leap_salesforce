# frozen_string_literal: true

RSpec.describe 'Any Record' do
  before { @contact = FactoryBot.create(:contact) }
  it 'retrieve individual' do
    contact = Contact.find
    expect(contact.last_name).to_not be_empty
  end
  it 'be checked it exists' do
    expect(Contact.any?).to be true
  end
  it 'retrieves list' do
    expect(Contact.list.size).to be > 0
  end
  after(:each) { @contact&.delete }
end

RSpec.describe 'Query Record' do
  it 'by id' do
    @contact = FactoryBot.create(:contact)
    retrieved_contact = Contact.find(Id: @contact.id)
    expect(retrieved_contact.id).to eq @contact.id
  end
  it 'using deprecated "get"' do
    @contact = FactoryBot.create(:contact)
    retrieved_contact = Contact.get(Id: @contact.id)
    expect(retrieved_contact.id).to eq @contact.id
  end
  it 'by past date' do
    objects = Contact.each_id_with CreatedDate: "<#{0.days.ago}"
    expect(objects.count).to be_instance_of Integer
  end
  it 'based on picklist' do
    @contact = FactoryBot.create(:contact, lead_source: Contact::LeadSource.web)
    email_contacts = Contact.ids_where LeadSource: Contact::LeadSource.web
    expect(email_contacts).to be_instance_of Array
  end
  it 'uses Contact::Fields module to extract fields' do
    @contact = FactoryBot.create(:contact) # Ensure there is a Contact when running against Sandbox
    expect(@contact).to respond_to :last_name
    expect(@contact.last_name).to be_instance_of String # last_name is method in Contact::Fields
  end
  it 'by exact date' do
    @contact = FactoryBot.create(:contact)
    contact_ids = Contact.each_id_with created_date: @contact.created_date
    expect(contact_ids.count).to be > 0
  end
  after(:each) { @contact&.delete }
end

RSpec.describe 'Comparators' do
  it 'handles less than or equal to' do
    @contact = FactoryBot.create(:contact)
    @contact = Contact.find LastModifiedDate: "<=#{0.day.ago}"
    expect(@contact).to be_successful
  end
  it 'use not' do
    @contact = Contact.create first_name: 'NOT ME PLEASE'
    contact_ids = Contact.ids_where first_name: '!NOT ME PLEASE'
    expect(contact_ids).not_to include @contact.id
  end
  it 'use IN' do
    @web = Contact.create lead_source: Contact::LeadSource.web
    @phone = Contact.create lead_source: Contact::LeadSource.phone_inquiry
    list = [Contact::LeadSource.web, Contact::LeadSource.phone_inquiry]
    contact_ids = Contact.ids_where lead_source: "IN#{list}"
    expect(contact_ids).to include @web.id
    expect(contact_ids).to include @phone.id
    @web&.delete
    @phone&.delete
  end
  it 'use NOT IN' do
    @web = Contact.create lead_source: Contact::LeadSource.web
    @phone = Contact.create lead_source: Contact::LeadSource.phone_inquiry
    list = [Contact::LeadSource.web]
    contact_ids = Contact.ids_where lead_source: "!IN#{list}"
    expect(contact_ids).not_to include @web.id
    expect(contact_ids).to include @phone.id
    @web&.delete
    @phone&.delete
  end
  it 'find based on related id' do
    @contact = Contact.create :account
    expect(Contact.where(account_id: @contact.account_id)[0].id).to eq @contact.id
  end
  it 'find where null' do
    @contact = Contact.create
    no_accounts = Contact.ids_where account_id: nil
    expect(no_accounts).to include @contact.id
  end
  context 'not null' do
    it 'using !null' do
      @contact = Contact.create salutation: Contact::Salutation.sample
      is_described = Contact.ids_where salutation: '!null'
      expect(is_described).to include @contact.id
    end
    it 'using !nil' do
      @contact = Contact.create salutation: Contact::Salutation.sample
      is_described = Contact.ids_where salutation: '!nil'
      expect(is_described).to include @contact.id
    end
  end
  it 'find for double' do
    @contact = FactoryBot.create(:contact, :coordinates)
    expect(Contact.ids_where(location_longitude: '2.0')).to include @contact.id
  end
  it 'find for boolean' do
    @contact = Contact.create
    expect(Contact.ids_where(deleted: false)).to include @contact.id
  end
  after(:each) { @contact&.delete }
end

RSpec.describe 'Related record' do
  it 'can detect if exist' do
    @contact = Contact.create :account
    expect(Contact.any?(account_id: @contact.account_id)).to be true
  end
  it 'can detect if not exist' do
    @contact = Contact.create
    expect(Contact.any?(account_id: @contact.id)).to be false
  end
  after(:each) { @contact&.delete }
end
