# frozen_string_literal: true

RSpec.describe 'Case accessors' do
  subject(:accessors) { Case.accessors }
  it 'shows backend value' do
    expect(accessors[:case_number][:backend]).to eq 'CaseNumber'
  end
  it 'shows label' do
    expect(accessors[:asset_id][:label]).to eq 'Asset ID'
  end
  it 'shows type' do
    expect(accessors[:status][:type]).to eq 'picklist'
  end
  it 'shows related object' do
    expect(accessors[:account_id][:related_object]).to eq 'Account'
  end
end
