# frozen_string_literal: true

RSpec.describe 'Session' do
  it 'Can have a valid id and user id extracted' do
    soap_session = LeapSalesforce::Session.new LeapSalesforce::Users.list[0].username,
                                               LeapSalesforce.password,
                                               LeapSalesforce::Users.list[0].security_token
    expect(soap_session.session_id).not_to be nil
    expect(soap_session.user_id).not_to be nil
  end
end
