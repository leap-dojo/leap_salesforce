# frozen_string_literal: true

# This file demonstrates how simple creation, reading updating and deletion (CRUD)
# can be done through LeapSalesforce classes
# Depending on validation rules and mandatory fields this may or may not work

RSpec.describe 'Contact CRUD' do
  let(:uniq_value) { "UNIQ #{Faker::Name.first_name} #{Faker::Name.middle_name}" }
  let(:updated_value) { "New #{Faker::Name.first_name} #{Faker::Name.neutral_first_name}" }
  it 'Create using factory bot' do
    @contact = FactoryBot.create(:contact)
  end
  it 'Read contact by name' do
    FactoryBot.create(:contact, first_name: uniq_value)
    @contact = Contact.find(first_name: uniq_value)
    retrieved_first_name = @contact.first_name
    expect(retrieved_first_name).to eq uniq_value
  end
  it 'Updates contacts name through setter' do
    @contact = FactoryBot.create(:contact, first_name: uniq_value)
    @contact.first_name = updated_value
    expect(@contact.find.first_name).to eq updated_value
  end
  it 'Updates name through update method' do
    @contact = FactoryBot.create(:contact, first_name: uniq_value)
    @contact.update first_name: updated_value
    expect(@contact.first_name).to eq updated_value
  end
  after { @contact&.delete must_pass: true } # Delete data after each action.
end
