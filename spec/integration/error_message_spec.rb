# frozen_string_literal: true

RSpec.describe 'Error message' do
  it 'correct message when error occurs' do
    @contact = Contact.create first_name: 'a' * 100
    expect(@contact.error_message).to include 'data value too large'
  end
  it 'meaningful message when no error occurs' do
    @contact = Contact.create
    @contact.first_name = 'New Value'
    expect do
      @contact.error_message
    end.to raise_error LeapSalesforce::ResponseError, /No error message received/
  end
  context '.diagnose_error' do
    it 'returns error when error' do
      @contact = Contact.create first_name: 'a' * 100
      expect(@contact.diagnose_error).to include 'data value too large'
    end
    it 'describes response if no error' do
      @contact = Contact.create
      expect(@contact.diagnose_error).to include 'id'
    end
  end
  after { @contact&.delete }
end

RSpec.describe 'Error when find fails' do
  it 'clarifies for search' do
    expect do
      Contact.find first_name: 'Name that should never exist', last_name: 'Not there'
    end.to raise_error LeapSalesforce::RequestError, /:first_name=>"Name that should never/
  end
end
RSpec.describe 'Error after an incorrect update' do
  it 'raised on retrieve' do
    @contact = Contact.create
    @contact.first_name = 'a' * 100
    expect do
      @contact.first_name
    end.to raise_error LeapSalesforce::ResponseError, /Error with updating/
  end
  it 'raised on success_update' do
    @contact = Contact.create
    expect do
      @contact.success_update first_name: 'a' * 100
    end.to raise_error LeapSalesforce::ResponseError, /Error with updating/
  end
  after { @contact&.delete }
end
