# frozen_string_literal: true

RSpec.describe 'Creating Record' do
  let(:first_name) { Faker::Name.first_name }
  it 'step by step' do
    @contact = Contact.new
    @contact.first_name = first_name # Example using soql_element accessor
    @contact[:lastname] = Faker::Name.last_name # Example using []= method
    @contact.save!
    expect(@contact.successful?).to be true
    expect(@contact.first_name).to eq first_name
  end
  it 'setting params on new' do
    @contact = Contact.new first_name: first_name, last_name: Faker::Name.last_name
    @contact.save!
    expect(@contact.successful?).to be true
    expect(@contact.first_name).to eq first_name
  end
  it 'through factory' do
    @contact = FactoryBot.create(:contact)
    expect(@contact.successful?).to be true
  end
  it 'through factory on object itself' do
    @contact = Contact.create
    expect(@contact.successful?).to be true
  end
  it 'with trait' do
    @contact = Contact.create :account
    created_account = @contact.account_id # Using custom name
    expect(created_account).not_to be nil
    expect(Account.find(Id: created_account).id).to eq created_account
  end
  after(:each) { @contact&.delete } # Don't want to fill sandbox with data
end

RSpec.describe 'Unsetting value during create' do
  context '.unset' do
    it 'unsets default element' do
      @contact = FactoryBot.create(:contact, :no_last_name)
      puts @contact.request_parameters.body
      expect(@contact.request_parameters.body).not_to include 'LastName'
      expect(@contact.error_message).to eq 'Required fields are missing: [LastName]'
    end
    it 'raises error if value to unset not present' do
      @contact = FactoryBot.create(:contact)
      expect do
        @contact.unset = :NotSetYet
      end.to raise_error LeapSalesforce::RequestError, /NotSetYet/
    end
  end
end
