# frozen_string_literal: true

RSpec.describe Leaps::User do
  it 'can have password set' do
    expect do
      Leaps::Users[:extra].password = "TestPassword#{Time.now.to_i}#{Faker::Lorem.word}"
    end.not_to raise_exception
  end
  it 'can have password reset' do
    tmp_password = Leaps::Users[:extra].reset_password
    expect(tmp_password).not_to be_empty
  end
end
