# frozen_string_literal: true

# Tests for checking environment setup correctly

RSpec.describe 'Users' do
  context 'exists?' do
    it 'true if present' do
      expect(LeapSalesforce::Users.where(key: :admin)).to exist
    end
    it 'false when not' do
      user_not_exist = LeapSalesforce::Users.where(key: :not_there)
      expect(user_not_exist.exists?).to eq false
    end
  end
end
