# frozen_string_literal: true

RSpec.describe 'Limits' do
  let(:soql_limits) { SoqlData.limits }
  context 'Storage > 4MB' do
    it 'data storage' do
      expect(LeapSalesforce::Limits.remaining_for(:DataStorageMB)).to be > 4
    end
    it 'file storage' do
      expect(LeapSalesforce::Limits.remaining_for(:FileStorageMB)).to be > 4
    end
  end
end
