# frozen_string_literal: true

require 'leap_salesforce/generator/default'

RSpec.describe LeapSalesforce::Default::Factory do
  context '.value_for' do
    let(:class_name) { 'Contact' }
    let(:association) { 'association :account_id, factory: :account' }
    let(:find_recent_user) { %{User.find(CreatedDate: "<#{0.days.ago}").id} }
    let(:expected_salutation) { 'salutation { Contact::Salutation.sample }' }
    let(:expected_label_salutation) { 'label { Contact::Label.sample }' }
    it 'demonstrates retrieving current object' do
      field_used = { 'type' => 'reference', 'name' => 'OwnerId', 'label' => 'OwnerId' }
      expect(described_class.value_for(field_used, class_name)).to include find_recent_user
    end
    it 'demonstrates creating association' do
      field_used = { 'type' => 'reference', 'name' => 'AccountId', 'label' => 'AccountId',
                     'relationshipName' => 'Account' }
      expect(described_class.value_for(field_used, class_name)).to eq association
    end
    it 'creates reference to picklist' do
      field_used = { 'type' => 'picklist', 'name' => 'Salutation', 'label' => 'Salutation' }
      expect(described_class.value_for(field_used, class_name)).to eq expected_salutation
    end
    it 'uses label for picklist' do
      field_used = { 'type' => 'picklist', 'name' => 'Name', 'label' => 'Label' }
      expect(described_class.value_for(field_used, class_name)).to eq expected_label_salutation
    end
  end
end
