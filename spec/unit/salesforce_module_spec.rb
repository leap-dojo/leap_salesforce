# frozen_string_literal: true

RSpec.describe Salesforce do
  it 'can have limits read' do
    expect(Salesforce).to respond_to :limits
  end
  it 'can describe objects globally' do
    expect(Salesforce).to respond_to :global_describe
  end
end
