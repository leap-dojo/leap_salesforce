# frozen_string_literal: true

require 'leap_salesforce/generator/exe_helpers'

RSpec.describe LeapSalesforce::ExeHelpers do
  before { extend LeapSalesforce::ExeHelpers }
  context '.input_for' do
    it 'queries for input' do
      allow($stdin).to receive(:gets) { 'user@gmail.com' }
      expect(input_for('Username:')).to eq 'user@gmail.com'
    end
  end
end
