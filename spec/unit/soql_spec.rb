# frozen_string_literal: true

class DummyObject < SoqlData
  # @return [String] Simulate this defined through 'soql_element' method
  def created_date_element
    'CreatedDate'
  end
  class << self
    # Actually implemented within SoqlObjectDescribe
    # @return Same as param entered to allow ease of testing
    def type_for(field_name)
      field_name
    end

    # Mock field names for testing methods like .map_key
    def field_names
      %w[CreatedDate Id]
    end
  end
end

RSpec.describe LeapSalesforce::Soql do
  subject(:soql) { LeapSalesforce::Soql.new(DummyObject) }
  context '.extract_comparator' do
    it 'extracts >' do
      expect(soql.extract_comparator('>Today')).to eq %w[> Today]
    end
    it 'extracts <' do
      expect(soql.extract_comparator('<5')).to eq %w[< 5]
    end
    it 'extracts >=' do
      expect(soql.extract_comparator('>=Today')).to eq %w[>= Today]
    end
    it 'extracts !' do
      expect(soql.extract_comparator('!Tester')).to eq %w[!= Tester]
    end
    it 'handles string starting with I' do
      expect(soql.extract_comparator('India')).to eq %w[= India]
    end
    it 'extracts IN' do
      expect(soql.extract_comparator('IN["a", "b"]')).to eq ['IN', "('a', 'b')"]
    end
    it 'extracts !IN (NOT IN)' do
      expect(soql.extract_comparator('!IN["a c", "b"]')).to(
        eq(['NOT IN', "('a c', 'b')"])
      )
    end
  end
  context '.condition_for' do
    it 'handles before 1 day ago' do
      time = 1.day.ago
      result = soql.condition_for('datetime', "<=#{time}")
      expect(result).to start_with '<= '
      expect(result).not_to include "'"
      expect(result).to end_with time.to_s.to_zulu_date_string
    end
    it 'handles exact date' do
      time = Time.now
      result = soql.condition_for 'date', time
      expect(result).to start_with '= '
      expect(result).not_to include "'"
      expect(result).to end_with time.to_s.to_zulu_date_string
    end
    it 'handles id' do
      id = '0032v00002tINcMAAW'
      result = soql.condition_for 'id', id
      expect(result).to start_with '= '
      expect(result).to end_with "'#{id}'"
    end
    it 'handles not object' do
      id = '!TESTER'
      result = soql.condition_for 'string', id
      expect(result).to start_with '!= '
      expect(result).to end_with "'TESTER'"
    end
    it 'handles double' do
      result = soql.condition_for 'double', 2.0
      expect(result).to eq '= 2.0'
    end
    it 'handles boolean' do
      result = soql.condition_for 'boolean', false
      expect(result).to eq '= false'
    end
    it 'handles null' do
      result = soql.condition_for 'string', nil
      expect(result).to eq '= null'
    end
    it "handles single quote '" do
      result = soql.condition_for 'string', "UNIQ Dann O'Hara"
      expect(result).to eq "= 'UNIQ Dann O\'Hara'"
    end
  end
  context '.map_key' do
    it 'raises exception when not found' do
      expect { soql.map_key('unknown_field') }.to raise_error LeapSalesforce::RequestError
    end
    it 'finds field through field name element' do
      expect(soql.map_key('created_date')).to eq 'CreatedDate'
    end
    it 'finds field directly' do
      expect(soql.map_key(:CreatedDate)).to eq 'CreatedDate'
    end
  end
end

RSpec.describe SoqlGlobalObjectData do
  let(:soql) { LeapSalesforce::Soql.new(DummyObject) }
  context '.soql_lookup_filter' do
    context 'order_by' do
      it 'defaults' do
        expect(soql.soql_lookup_filter({})).to include 'ORDER BY CreatedDate'
      end
      it 'customized' do
        expect(soql.soql_lookup_filter(order_by: 'CustomField')).to include 'ORDER BY CustomField'
      end
    end
    it 'custom limit' do
      expect(soql.soql_lookup_filter(limit: 10)).to end_with ' LIMIT 10'
    end
  end
end
