# frozen_string_literal: true

require 'leap_salesforce/generator/soql_objects'

RSpec.describe LeapSalesforce::Generator::SoqlObjects do
  let(:class_name) { 'ClassThatDoesNotExist' }
  it '.ensure_class_defined creates class' do
    generator = subject
    generator.instance_variable_set(:@soql_object, LeapSalesforce::SoqlObject.new(class_name))
    generator.ensure_class_defined
    soql_class = generator.instance_variable_get(:@soql_class)
    expect(soql_class.class).to be_instance_of Class
    expect(soql_class.to_s).to eq class_name
  end
end
