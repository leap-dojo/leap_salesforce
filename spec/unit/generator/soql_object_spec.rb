# frozen_string_literal: true

require 'leap_salesforce/soql_object'
# Below is description of YAML used in .leap_salesforce.yml

RSpec.shared_examples_for 'default enum value' do
  it 'create_enum = true' do
    expect(soql_object.create_enum).to eq true
  end
end

RSpec.shared_examples_for 'default backend class names' do
  it 'class name as key' do
    expect(soql_object.class_name).to eq 'ClassName'
  end
  it 'backend name as value' do
    expect(soql_object.backend_name).to eq 'BackendName'
  end
  it 'reference using class name' do
    expect(soql_object.reference).to eq 'class_name'
  end
end

RSpec.describe 'String of Backend table provided' do
  let(:description) { 'BackendSoqlName' }
  subject(:soql_object) { LeapSalesforce::SoqlObject.new description }
  it 'has backend and class name set the same' do
    expect(soql_object.backend_name).to eq description
    expect(soql_object.class_name).to eq description
  end
  it 'has snakecase reference' do
    expect(soql_object.reference).to eq 'backend_soql_name'
  end
  it_behaves_like 'default enum value'
end

RSpec.describe 'Hash with ClassName:BackendName' do
  let(:description) { { 'ClassName' => 'BackendName' } }
  subject(:soql_object) { LeapSalesforce::SoqlObject.new description }
  it_behaves_like 'default backend class names'
  it_behaves_like 'default enum value'
end

RSpec.describe 'Hash with create_enum: false' do
  let(:description) do
    { 'ClassName' => 'BackendName',
      'create_enum' => false }
  end
  subject(:soql_object) { LeapSalesforce::SoqlObject.new description }
  it_behaves_like 'default backend class names'
  it 'has create enum = false' do
    expect(soql_object.create_enum).to eq false
  end
end

RSpec.describe 'Hash excluding specific enums' do
  let(:description) do
    { 'ClassName' => nil,
      'create_enum' => { 'exclude' => %w[Timezone CaseHistory] } }
  end
  subject(:soql_object) { LeapSalesforce::SoqlObject.new description }
  it 'class name as key' do
    expect(soql_object.class_name).to eq 'ClassName'
  end
  it 'backend name as value' do
    expect(soql_object.backend_name).to eq 'ClassName'
  end
  it 'reference using class name' do
    expect(soql_object.reference).to eq 'class_name'
  end
  context 'excludes' do
    it 'direct picklist name' do
      expect(soql_object.excludes?('Timezone')).to be true
    end
    it 'class name' do
      expect(soql_object.excludes?('Case History')).to be true
    end
    it 'not unrelated picklist' do
      expect(soql_object.excludes?('Unrelated')).to be false
    end
  end
end
