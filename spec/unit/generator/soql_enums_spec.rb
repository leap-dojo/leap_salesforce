# frozen_string_literal: true

require 'leap_salesforce/generator/soql_enums'

class MockSoqlData
  # @return [Array]
  attr_accessor :picklists

  def initialize(picklists)
    self.picklists = picklists
  end

  def create_enum?
    @create_enums
  end

  # Imitate existing object
  def to_s
    'Case'
  end

  def picklist_for(name)
    ["#{name}_1", "#{name}_2"]
  end
end

RSpec.describe LeapSalesforce::Generator::SoqlEnums do
  subject(:enum_gen) { LeapSalesforce::Generator::SoqlEnums.new }
  it 'raises setup error on empty list' do
    allow(LeapSalesforce).to receive(:soql_objects) { [] }
    expect do
      enum_gen.create
    end.to raise_error LeapSalesforce::SetupError
  end
  it 'generates picklist file based on content' do
    allow(LeapSalesforce).to receive(:soql_objects) { YAML.load('["Case"]') }
    enum_gen.instance_variable_set(:@soql_data_object, MockSoqlData.new(['test_list']))
    enum_gen.instance_variable_set(:@entity_name, 'Case')

    enum_gen.generate_picklist_file('test_list')
    created_content = File.read('spec/support/metadata/enum/case/test_list.rb')
    expect(created_content).to include '@test_list_1 = "test_list_1"'
  end
end
