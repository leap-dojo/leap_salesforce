# frozen_string_literal: true

require 'leap_salesforce/generator/generator'

RSpec.describe LeapSalesforce::Generator do
  before { extend LeapSalesforce::Generator }
  context '.template_loc' do
    it 'should default to leap_salesforce folder' do
      expect(template_loc('Gemfile.erb')).to end_with 'lib/leap_salesforce/generator/templates/Gemfile.erb'
    end
    it 'should allow external gem folder' do
      expect(template_loc('Gemfile.erb', folder: 'test')).to eq 'test/templates/Gemfile.erb'
    end
  end
  it '.read_template reads content' do
    expect(read_template('Gemfile.erb', binding)).to include "gem 'rspec'"
  end
end
