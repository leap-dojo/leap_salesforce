# frozen_string_literal: true

RSpec.describe String do
  context '.to_ruby_friendly' do
    it 'removes spaces' do
      expect('Test Space'.to_ruby_friendly).to eq 'Test_Space'
    end
    it 'removes mathematical operators' do
      expect('Test+Name*Works-Another/5'.to_ruby_friendly).to eq 'TestNameWorksAnother5'
    end
    it 'converts single zero' do
      expect('0'.to_ruby_friendly).to eq 'zero'
    end
    it 'handles bigger num' do
      expect('44'.to_ruby_friendly).to eq 'four4'
    end
    it 'removes :' do
      expect('Test:Name'.to_ruby_friendly).to eq 'TestName'
    end
    it 'removes currency' do
      expect('AmountInCurrency$€£¥'.to_ruby_friendly).to eq 'AmountInCurrency'
    end
    it 'removes quotes and brackets' do
      expect(%q('String' And "Double").to_ruby_friendly).to eq 'String_And_Double'
    end
    it 'replaces logical operators' do
      expect('one|two&three=four'.to_ruby_friendly).to eq 'one_two_three_four'
    end
    it 'replaces > and <' do
      expect('three>two<four'.to_ruby_friendly).to eq 'three_gt_two_lt_four'
    end
    it 'handles macrons' do
      expect('māori'.to_ruby_friendly).to eq 'maori'
    end
    it 'handles whitespace' do
      white_spaced = "test \t \n   together"
      expect(white_spaced.to_ruby_friendly).to eq 'test_together'
    end
    it 'removes comment char' do
      expect('has a # in it'.to_ruby_friendly).to eq 'has_a__in_it'
    end
    it 'handles other char' do
      expect("tick \u2713 \"{\t}\"".to_ruby_friendly).to eq 'tick___'
    end
  end
end

RSpec.describe String do
  context '.to_class_name' do
    it 'converts from snakecase' do
      expect('snakey_case'.to_class_name).to eq 'SnakeyCase'
    end
    it 'handles custom object' do
      expect('Role__c'.to_class_name).to eq 'RoleC'
    end
    it 'removes whitespace and underscores' do
      expect(%q('String' And "Double").to_class_name).to eq 'StringAndDouble'
    end
    it 'camelizes number at start' do
      expect('5Otherredirections'.to_class_name).to eq 'FiveOtherredirections'
    end
  end
end

RSpec.describe String do
  context '.to_key_name' do
    it 'converts to camelcase' do
      expect('Sentence In Camel'.to_key_name).to eq 'sentence_in_camel'
    end
    it 'handles numbers at beginning' do
      expect('5.A Name'.to_key_name).to eq 'five_a_name'
    end
    it 'cleans up too many underscores' do
      expect('a_____b'.to_key_name).to eq 'a_b'
    end
    it 'handles $ + number' do
      expect('$5'.to_key_name).to eq 'five'
    end
    it 'handles purely currency' do
      expect('$'.to_key_name).to eq 'x'
    end
    it 'tidies where _ at start' do
      expect('<50'.to_key_name).to eq 'lt_50'
    end
  end
end

RSpec.describe String do
  context '.to_key_name_for' do
    it 'handles unique value' do
      hash = {}
      expect('A Unique value'.to_key_name_for(hash)).to eq 'a_unique_value'
    end
    it 'handles keys that are converted to look the same' do
      hash = { 'this_is_a_test' => 'val' }
      expect('This is, a test'.to_key_name_for(hash)).to eq 'this_is_a_test_1'
    end
    it 'handles 2 duplicates' do
      hash = { 'this_is_a_test' => 'val', 'this_is_a_test_1' => 'val2' }
      expect('This is, a test'.to_key_name_for(hash)).to eq 'this_is_a_test_2'
    end
  end
end

RSpec.describe String do
  context '.to_soql_array' do
    let(:single) { ['one element'].to_s }
    it 'handles single element' do
      expect(single.to_soql_array).to eq "('one element')"
    end
    it 'converts string like array into correct format' do
      three_words = ['a', 'two words', 'three'].to_s
      expect(three_words.to_soql_array).to eq "('a', 'two words', 'three')"
    end
  end
end

RSpec.describe String, 'compatible with other gems' do
  context 'rubyzip' do
    it 'not respond to seek' do
      expect(String.new).not_to respond_to :seek
    end
  end
end

RSpec.describe String, '.unused_ruby_name' do
  it 'converts methods defined on SoqlData' do
    expect('status_code'.unused_ruby_name).to eq 'my_status_code'
  end
end
