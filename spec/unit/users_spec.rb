# frozen_string_literal: true

RSpec.describe 'Soql User from Leaps::User' do
  before(:each) do
    ENV['SF_USERNAME'] = 'samuel.garratt@brave-otter-ttxype.com'
    module Leaps
      Users.add [:admin, 'samuel.garratt@brave-otter-ttxype.com']
    end
  end
  it '.soql_object returns User object' do
    admin_user = Leaps::Users.where(username: 'samuel.garratt@brave-otter-ttxype.com')
    puts admin_user.inspect
    expect(admin_user.soql_object).to be_instance_of User
  end
  it '.first_name retrieved through lookup' do
    user = Leaps::Users.where(username: 'samuel.garratt@brave-otter-ttxype.com')
    expect(user.first_name).to eq 'Samuel'
  end
end

RSpec.describe 'adding users' do
  before do
    @prev_list = LeapSalesforce::Users.list
    LeapSalesforce::Users.list = []
  end
  after do
    LeapSalesforce::Users.list = @prev_list # Reset variable to how it was
  end
  subject { LeapSalesforce::Users }
  it 'added through user object' do
    subject.add LeapSalesforce::User.new :admin, description: 'test@testemail.com'
    expect(subject.list.count).to eq 1
    user = subject.list[0]
    expect(user.key).to eq :admin
  end
  it 'added through Array' do
    subject.add [:arr, 'test@testemail.com']
    expect(subject.list.count).to eq 1
    user = subject.list[0]
    expect(user.key).to eq :arr
  end
end

RSpec.describe 'Querying users' do
  before do
    @prev_list = LeapSalesforce::Users.list
    LeapSalesforce::Users.list = []
    module LeapSalesforce
      Users.add [:cet, 'cet@<%= LeapSalesforce.environment %>.email.com', { description: 'Central Enquiries team User' }]
      Users.add [:admin, 'admin@<%= LeapSalesforce.environment %>.email.com', 'System Admin User']
    end
  end
  after { LeapSalesforce::Users.list = @prev_list }
  subject { LeapSalesforce::Users }
  it 'found through key' do
    user = subject.where key: :cet
    expect(user.key).to eq :cet
  end
  it 'found through default' do
    user = subject.where :default
    expect(user.key).to eq :cet
  end
  it 'found through username full match' do
    user = subject.where username: "admin@#{LeapSalesforce.environment}.email.com"
    expect(user.username).to eq "admin@#{LeapSalesforce.environment}.email.com"
  end
  it 'found through username partial match' do
    user = subject.where username: /admin/
    expect(user.username).to eq "admin@#{LeapSalesforce.environment}.email.com"
  end
  it 'found through description with string' do
    user = subject.where description: /Enquiries/
    expect(user.username).to eq "cet@#{LeapSalesforce.environment}.email.com"
  end
  it 'found through description with hash' do
    user = subject.where description: /System Admin/
    expect(user.username).to eq "admin@#{LeapSalesforce.environment}.email.com"
  end
  it 'has default of first user' do
    expect(LeapSalesforce.api_user).to start_with @prev_list[0].username
  end
  it 'has helpful error when not found' do
    expect do
      subject.where key: :not_found
    end.to raise_error LeapSalesforce::UserError, /:not_found/
  end
  context '.any?' do
    it 'true if present' do
      expect(subject.any?(key: :admin)).to eq true
    end
    it 'false if not' do
      expect(subject.any?(key: :not_found)).to eq false
    end
  end
end

RSpec.describe 'Assigning user' do
  before do
    @prev_list = LeapSalesforce::Users.list
    LeapSalesforce::Users.list = []
    module Leaps
      Users.add [:cet, 'cet@<%= LeapSalesforce.environment %>.email.com', { description: 'Central Enquiries team User' }]
      Users.add [:payroll, 'payroll@<%= LeapSalesforce.environment %>.email.com', { description: 'Payroll User' }]
      Users.add [:admin, 'admin@<%= LeapSalesforce.environment %>.email.com', { description: 'System Admin User' }]
    end
  end
  after do
    Leaps::Users.list = @prev_list
    ENV['SF_USERNAME'] = 'samuel.garratt@brave-otter-ttxype.com'
  end
  subject { Leaps::Users }
  context 'using' do
    it :default do
      Leaps.api_user = :default
      expect(Leaps.api_user).to start_with 'cet'
    end
    it Symbol, 'checks within key' do
      Leaps.api_user = :admin
      expect(Leaps.api_user).to start_with 'admin'
    end
    it Regexp, 'checks within desc' do
      Leaps.api_user = /Payroll/
      expect(Leaps.api_user).to start_with 'payroll'
    end
    it Hash do
      Leaps.api_user = { description: /Payroll/ }
      expect(Leaps.api_user).to start_with 'payroll'
    end
    it LeapSalesforce::User do
      Leaps.api_user = Leaps::Users.where :payroll
      expect(Leaps.api_user).to start_with 'payroll'
    end
  end
  context '.execute_as_if_present' do
    let(:current_user) { Leaps.api_user }
    it 'uses current user when not exist' do
      subject.execute_as_if_present key: :not_found do
        expect(Leaps.api_user).to eq current_user
      end
    end
    it 'changes user if they do exist' do
      Leaps.api_user = { key: :cet }
      subject.execute_as_if_present key: :admin do
        expect(LeapSalesforce.api_user).to eq LeapSalesforce::Users.where(key: :admin).username
      end
      expect(LeapSalesforce.api_user).to eq LeapSalesforce::Users.where(key: :cet).username
    end
  end
  context '.execute_as' do
    it 'changes user if they do exist' do
      Leaps.api_user = { key: :cet }
      subject.execute_as key: :admin do
        expect(LeapSalesforce.api_user).to eq LeapSalesforce::Users.where(key: :admin).username
      end
      expect(LeapSalesforce.api_user).to eq LeapSalesforce::Users.where(key: :cet).username
    end
    it 'raises errors when not exist' do
      expect do
        subject.execute_as(key: :not_found) { Leaps.api_user }
      end.to raise_error LeapSalesforce::UserError
    end
  end
end

RSpec.describe 'Iterate over users' do
  before do
    @prev_list = LeapSalesforce::Users.list
    LeapSalesforce::Users.list = []
    module Leaps
      Users.add [:cet, 'cet@<%= LeapSalesforce.environment %>.email.com', { description: 'Central Enquiries team User' }]
      Users.add [:payroll, 'cet_manager@<%= LeapSalesforce.environment %>.email.com', { description: 'CET Manager' }]
      Users.add [:admin, 'admin@<%= LeapSalesforce.environment %>.email.com', { description: 'System Admin User' }]
    end
  end
  after do
    Leaps::Users.list = @prev_list
    ENV['SF_USERNAME'] = 'samuel.garratt@brave-otter-ttxype.com'
  end
  it '.each' do
    @count = 0
    Leaps::Users.each { @count += 1 }
    expect(@count).to eq 3
  end
  it '.each with filter' do
    @count = 0
    Leaps::Users.each(username: /cet/) { @count += 1 }
    expect(@count).to eq 2
  end
  it '.where all true' do
    result = Leaps::Users.where(username: /cet/, all: true)
    expect(result).to be_instance_of Array
    expect(result.count).to eq 2
  end
end

RSpec.describe 'Empty username' do
  before do
    @prev_list = LeapSalesforce::Users.list
    LeapSalesforce::Users.list = []
    LeapSalesforce.instance_variable_set(:@api_user, nil)
  end
  after do
    Leaps::Users.list = @prev_list
    ENV['SF_USERNAME'] = 'samuel.garratt@brave-otter-ttxype.com'
  end
  it 'raises error' do
    ENV['SF_USERNAME'] = ''
    expect do
      puts LeapSalesforce.api_user
    end.to raise_error LeapSalesforce::SetupError
  end
end

RSpec.describe 'Extracting token' do
  before do
    @prev_list = LeapSalesforce::Users.list
    LeapSalesforce::Users.list = []
    module LeapSalesforce
      Users.add [:no_token, 'cet@<%= LeapSalesforce.environment %>.email.com', { description: 'Central Enquiries team User' }]
      ENV['admin_token'] = 'admin_sec_token'
      Users.add [:admin, 'admin@<%= LeapSalesforce.environment %>.email.com']
    end
  end
  after do
    Leaps::Users.list = @prev_list
    ENV['SF_USERNAME'] = 'samuel.garratt@brave-otter-ttxype.com'
  end
  it 'handles no security token' do
    user = Leaps::Users.where key: :no_token
    expect(user.security_token).to be nil
  end
  it 'extracts token implicitly based no key name' do
    user = Leaps::Users.where key: :admin
    expect(user.security_token).to eq 'admin_sec_token'
  end
end
