# frozen_string_literal: true

RSpec.describe 'Sfdx' do
  context 'SCRATCH_ACCESS_TOKEN and SCRATCH_INSTANCE_URL manually set' do
    before do
      ENV['SFDX_ACCESS_TOKEN'] = 'Dummy Token'
      ENV['SFDX_INSTANCE_URL'] = 'Dummy/InstanceUrl/'
      # Nullify cache
      Leaps.sfdx = true
      Leaps::Auth.instance_url = nil
      Leaps::Auth.access_token = nil
    end
    after do
      ENV['SFDX_ACCESS_TOKEN'] = nil
      ENV['SFDX_INSTANCE_URL'] = nil
      # Nullify cache
      Leaps.sfdx = false
      Leaps::Auth.instance_url = nil
      Leaps::Auth.access_token = nil
    end
    it 'retrieves instance url' do
      expect(Leaps::Auth.instance_url).to eq 'Dummy/InstanceUrl'
    end
    it 'retrieves access token' do
      expect(Leaps::Auth.access_token).to eq 'Dummy Token'
    end
  end
end
