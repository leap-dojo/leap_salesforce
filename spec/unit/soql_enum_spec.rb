# frozen_string_literal: true

RSpec.describe SoqlEnum do
  let(:enum_name) { Contact::LeadSource }
  it 'can retireve all values' do
    expect(enum_name.values).to be_instance_of Array
    expect(enum_name.values).to include 'Web'
  end
  it 'can retrieve a sample' do
    expect(enum_name.sample).to be_instance_of String
  end
  it 'can retrieve referenced value' do
    expect(enum_name.web).to eq 'Web'
  end
end
